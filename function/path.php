<?php
	if (!isset($_GET['mod']) && !isset($_GET['act'])) {
		$mod = 'home';
		$act = 'home';
	}
	elseif (!isset($_GET['act'])) {
		$act = 'home';
	}
	else {
		$mod = $_GET['mod'];
		$act = $_GET['act'];		
	}
	
	$dir = "module/".$mod."/".$act.'.php';
	
	if (!file_exists($dir) and $mod != "ajax" ) {
		header('Location: error.php');
	}
?>