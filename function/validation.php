<?php
	/*
	$message[] = check_empty($_POST['txtuser'],"Bạn chưa nhập nội dung vào Form");
	$message[] = check_no_matches($_POST['txtpass'],$_POST['txtpass2'],"Mật khẩu và mật khẩu xác nhận không được giống nhau");	
	$message[] = check_matches($_POST['txtpass'],$_POST['txtpass2'],"Mật khẩu và mật khẩu xác nhận phải giống nhau");		
	$message[] = check_email($_POST['txtemail'],"Email không hợp lệ, vui lòng nhập lại.");
	$message[] = check_number($_POST['txtemail'],"Vui lòng nhập số");	
	$message[] = check_value_ready($_POST['txtuser'],"Nội dung đã có, vui lòng nhập nội dung khác.);
	
	check_alert($message); 			--> Alert Error xong roi Back Page
	check_alert($message,$link);	--> Alert Error xong roi nhay toi trang $link
	
	if($check_valid($message)== FALSE )   	--> Tu kiem tra de xu ly tuy theo Error Yes hay No
	{
		echo $message; var_dump($message); 
		js_alert($message);js_back();exit;
	}
	else
	{
		your code here...
	}
	
	check_value_ready($sql); --> Khong co ket qua tra ve EXIT luon
	
	*/
	

//--- FUNCTION dung cac lenh Javascript thong dung	

	function js_redirect($url)
	{
		print("<script type='text/javascript'>document.location.href='$url'</script>");
	}
	
	function js_alert($text)
	{
		print("<script type='text/javascript'>alert('$text')</script>");
	}
	
	function js_back()
	{
		print("<script type='text/javascript'>history.back();</script>");	
	}
	
	function js_reload()
	{
		print("<script type='text/javascript'>window.location.reload();</script>");	
	}	
	
	function js_alert_redirect($text,$url)
	{
		print("<script type='text/javascript'>alert('$text')</script>");
		print("<script type='text/javascript'>document.location.href='$url'</script>");
	}
		

//--- Ham kiem tra co phai la Email hay khong --- 
	function checkEmail($email)
	{
		return preg_match('/^\S+@[\w\d.-]{2,}\.[\w]{2,6}$/iU', $email) ? TRUE : FALSE;
	}

//--- Function xu ly $error tra ve --> $message = $error

	function check_valid($message){
		if(is_array($message))
		{
			$message = array_filter($message);
			$message = implode("-",$message);
		}
		
		if($message!="")
		{
			$report = FALSE;
		}
		else
		{
			$report = TRUE;
		}
		return $report;
	}
	
	//--- Neu co $error thi ALERT --> go BACK hoac go to $link
	function check_alert($message,$link=""){
		if(is_array($message))
		{
			$message = array_filter($message);
			$message = implode("\\n",$message);
		}
		if($message!="")
		{
			js_alert($message);
			
			if($link=="")
			{
				js_back();
			}
			else
			{
				js_redirect($link);
			}
			exit;
		}
	}

//----- KIEM TRA VALIDATION CUA DU LIEU ----------------------	
	
	//--- Khong co gia tri gi het thi tra ve $error
	function check_empty($data,$error){
		if($data == NULL )
		{
			return $error;
		}
	}
	
	//--- Bat buoc 2 BIEN phai khac nhau, giong nhau thi tra ve $error
	function check_no_matches($data1,$data2,$error){
		if($data1 == $data2)
		{
			return $error;
		}
	}
	
	//--- Bat buoc 2 BIEN phai giong nhau, khac nhau thi tra ve $error
	function check_matches($data1,$data2,$error){
		if($data1 != $data2)
		{
			return $error;
		}
	}
	
	//--- Khong phai Email thi tra ve $error
	function check_email($data,$error){
		$rule="/^[a-zA-Z]{1}[a-zA-Z0-9._]+\@[a-zA-Z0-9]{2,}\.[a-zA-Z.]{2,}$/";
		if(!preg_match($rule,$data))
		{
			return $error;
		}
	}
	
	//--- Khong phai la So Number thi tra ve $error
	function check_number($data,$error){
		if(is_int($data))
		{
			return $error;
		}
	}	
	
	//--- Khong phai la Money (gom So va .,) thi tra ve $error
	function check_money($data,$error){
		$rule="/^[0-9\.\,]$/";
		if(!preg_match($rule,$data))
		{
			return $error;
		}
	}
	
	//--- Khong phai la DATE thi tra ve $error
	function check_date($data,$error){
		$rule="/^[0-9]{2,4}-[0-9]{2}-[0-9]{2,4}$/";
		if(!preg_match($rule,$data))
		{
			return $error;
		}
	}	
	
//---- KIEM TRA XEM RECORD DO CO TON TAI TRONG DATABASE KHONG ----
	
	//--- Kiem tra neu khong co Record --> $error
	function check_not_ready($table,$where,$error){
		$sql = "SELECT `id` FROM `$table` WHERE $where" ;
		$row = getData($sql);
		if($row["id"]=="")
		{
			return $error;
		}
	}

	//--- Kiem tra neu co Record --> $error
	function check_value_ready($table,$where,$error){
		$sql = "SELECT `id` FROM `$table` WHERE $where" ;
		$row = getData($sql);
		if($row["id"]!="")
		{
			return $error;
		}
	}

	//--- Kiem tra neu khong co Record --> $exit
	function check_not_exit($table,$where,$error){
		$sql = "SELECT `id` FROM `$table` WHERE $where" ;
		$row = getData($sql);
		if($row["id"]=="")
		{
			exit;
		}
	}
		
	//--- Kiem tra neu co Record --> exit
	function check_value_exit($sql){
		$sql = "SELECT `id` FROM `$table` WHERE $where" ;
		$row = getData($sql);
		if($row["id"]!="")
		{
			exit;
		}
	}

	//--- Cat chuoi de lam title
	function limit_text($text,$maxlen){
		$sentenceSymbol=array(" ");  // điểm kết thúc câu
		$text=strip_tags($text,"<br /><br/><br><b><i><font>"); // những tag muốn giữ lại
		if(strlen($text)>$maxlen) {
		
			for ($i=$maxlen; $i>0; $i--)  {
				$ch=substr($text,$i,1);
				if (in_array($ch,$sentenceSymbol)){
					$pos=$i;
					$i=0;
				}
			}
			
			$temp=substr($text,0,@$pos)."...";
		}
		else
			$temp=$text;
		
		return $temp;
	}
	
	function limit_char($text,$maxlen){
		if(strlen($text)>$maxlen)
			$temp=substr($text,0,$maxlen-3)."...";
		else
			$temp=$text;
		
		return $temp;
	}
	
	function limit_char_check_blank($text,$maxlen){

		$result = explode(" ",$text);
		if(count($result)>1)
			$temp = limit_text($text,$maxlen);
		else
			$temp = limit_char($text,$maxlen);
		return $temp;
	}

	//--- Ham chuyen doi ngay thang	
	function date_to_mysql($date)
	{
		$a = explode('-',$date);
		if(sizeof($a)==3)
		{
			return ($a[2].'-'.$a[1].'-'.$a[0]);
		}
	}
	function mysql_to_date($date)
	{
		$a = explode('-',$date);
		if(sizeof($a)==3)
		{
			return ($a[2].'-'.$a[1].'-'.$a[0]);
		}
	}		
	
	//--- Doi tieng Viet co dau > khong dau
	function strip_url($string)
	{
		$marTViet = array(
			"à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă","ằ","ắ","ặ","ẳ","ẵ",
			"è","é","ẹ","ẻ","ẽ","ê","ề","ế","ệ","ể","ễ",
			"ì","í","ị","ỉ","ĩ",
			"ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ","ờ","ớ","ợ","ở","ỡ",
			"ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ","ü",
			"ỳ","ý","ỵ","ỷ","ỹ",
			"đ",
			"A","À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă","Ằ","Ắ","Ặ","Ẳ","Ẵ",
			"B","C","D","Đ","F","J","H","G","K","L","M","N","P","Q","R","S","T","V","W","X","Z",
			"E","È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
			"I","Ì","Í","Ị","Ỉ","Ĩ",
			"O","Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ","Ờ","Ớ","Ợ","Ở","Ỡ",
			"U","Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
			"Y","Ỳ","Ý","Ỵ","Ỷ","Ỹ",
			" ","_","&",".","~","`","!","@","#","$","%","^","*","(",")","+","=",",","<",">","|","{","}","[","]",":",";","?","--","---","'","/"
		);
		
		$marKoDau = array(
			"a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a",
			"e","e","e","e","e","e","e","e","e","e","e",
			"i","i","i","i","i",
			"o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o",
			"u","u","u","u","u","u","u","u","u","u","u","u",
			"y","y","y","y","y",
			"d",
			"a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a","a",
			"b","c","d","d","f","j","h","g","k","l","m","n","p","q","r","s","t","v","w","x","z",
			"e","e","e","e","e","e","e","e","e","e","e","e",
			"i","i","i","i","i","i",
			"o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o",
			"u","u","u","u","u","u","u","u","u","u","u","u",
			"y","y","y","y","y","y",
			"-","-","-","-","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""
		);
		
		$result = str_replace($marTViet,$marKoDau,$string);
		
		return $result;
	}	
	
	function check_slug($slug,$table)
	{
		$sql = "SELECT COUNT(*) AS `count` FROM `$table` WHERE `slug` = '$slug'";
		$result = getCount($sql);
		if($result > 0)
		{
			$content = $slug."-1";
			return check_slug($content,$table);
		}
		else
		{
			return $slug;
		}
	}	
?>