<?php

class IMAGE_UPLOAD_RESIZE {
	
	var $url;
	var $type;
	var $width;
	var $height;
	var $thumb;
	var $source;
	var $path;
	var $ext;
	
	function IMG_Resize_Auto($width , $height , $file , $path, $from) {
		$this->IMG_check_type($from);
		$this->url		= $file;
		$this->width	= $width;
		$this->path		= $path.'.'.$this->ext;
		list($cur_width,$cur_height) = getimagesize($this->url);
		$ratio = $cur_width/$cur_height;
		if ($cur_width != $this->width) {
			$this->height	= $this->width / $ratio;
			if ($this->height > $height) {
				$this->height	=	$height;
				$this->width	=	$this->height * $ratio;
			}	
			$this->IMG_Load();
			imagecopyresampled($this->thumb, $this->source, 0, 0, 0, 0, $this->width, $this->height, $cur_width, $cur_height);
			$this->IMG_Output();
		}
		chmod($this->url,0755);			
	}
	
	function IMG_Resize_Width($width , $file , $path, $from) {
		$this->IMG_check_type($from);
		$this->width	= $width;
		$this->url		= $file;		
		$this->path		= $path.'.'.$this->ext;
		list($cur_width,$cur_height) = getimagesize($this->url);
		$ratio = $cur_width/$cur_height;
		if ($cur_width > $this->width) {
			$this->height	= $this->width / $ratio;
			$this->IMG_Load();
			imagecopyresampled($this->thumb, $this->source, 0, 0, 0, 0, $this->width, $this->height, $cur_width, $cur_height);
			$this->IMG_Output();
		}
		chmod($this->url,0755);				
	}
	
	function IMG_check_type($from) {
		$this->type = $_FILES[$from]['type'];
		switch ($this->type) {
			case 'image/pjpeg' :
				//header('Content-type: image/jpeg');
				$this->ext = 'jpg';
				break;
			case 'image/jpeg' :
				//header('Content-type: image/jpeg');
				$this->ext = 'jpg';
				break;
			case 'image/gif' :
				//header('Content-type: image/gif');
				$this->ext = 'gif';
				break;
			case 'image/x-png' :
				//header('Content-type: image/x-png');
				$this->ext = 'png';
				break;
			default:
				$this->ext = 'un_known';
				break;
		}
	}
	
	function IMG_Load() {
		
		$this->thumb = imagecreatetruecolor($this->width, $this->height);
		
		switch ($this->type) {
			case 'image/pjpeg' :
				$this->source = imagecreatefromjpeg($this->url);
				break;
			case 'image/jpeg' :
				$this->source = imagecreatefromjpeg($this->url);
				break;
			case 'image/gif' :
				$this->source = imagecreatefromgif($this->url);
				 imagealphablending($this->thumb, false);
				  imagesavealpha($this->thumb,true);
				  $transparent = imagecolorallocatealpha($this->thumb, 255, 255, 255, 127);
				  imagefilledrectangle($this->thumb, 0, 0, $this->width, $this->height, $transparent);
				break;
			case 'image/x-png' :
				$this->source = imagecreatefrompng($this->url);
				 imagealphablending($this->thumb, false);
				  imagesavealpha($this->thumb,true);
				  $transparent = imagecolorallocatealpha($this->thumb, 255, 255, 255, 127);
				  imagefilledrectangle($this->thumb, 0, 0, $this->width, $this->height, $transparent);
				break;	
		}	
	}
	
	function IMG_Output() {
		switch ($this->type) {
			case 'image/pjpeg' :
				imagejpeg($this->thumb,$this->path,100);	
				break;
			case 'image/jpeg' :
				imagejpeg($this->thumb,$this->path,100);	
				break;
			case 'image/gif' :
				imagegif($this->thumb,$this->path);	
				break;	
			case 'image/x-png' :
				imagepng($this->thumb,$this->path);	
				break;
				
		}
	}
	
	function IMG_Save($dir,$key,$from) {
		$file = explode(".",$_FILES[$from]['name']);
		$name = strtolower($file[count($file)-1]);
		$this->url  = $dir . $key.'.'.$name;
		move_uploaded_file($_FILES[$from]['tmp_name'], $this->url);
		chmod($this->url,0755);	
		return $this->url;	
	}

}
	
?>