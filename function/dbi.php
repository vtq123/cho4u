﻿<?php

	define("DBHOST","localhost");
	define("DBNAME","cho4u");
	define("DBUSER","root");
	define("DBPASS","");	
	
	//-- Xu ly cau Query --
	function myQuery($sql,&$result)
	{
		$conn = mysql_connect(DBHOST,DBUSER,DBPASS) or die("Unable to connect to Database");
		$db = mysql_select_db(DBNAME,$conn) or die("Unable to select the database");
		$result = mysql_query($sql) or die("Illegal query. $sql");
		mysql_close($conn) or die("Unable to close the database");
	}
	
	//-- Dem tong so Record ---
	function getCount($sql)
	{
		myQuery($sql,$result);
		while($row = mysql_fetch_assoc($result))
		{
			$count = $row["count"];
		}
		mysql_free_result($result);
		return $count; 
	}	
	
	//-- Lay ra info [id] [name] cua 1 record ---
	function getData($sql)
	{
		myQuery($sql,$result);
		$row=mysql_fetch_assoc($result);
		mysql_free_result($result);
		return $row;
	}
	
	//-- Lay ra info [id] [name] cua nhieu record ---	
	function getDataInfo($sql)
	{
		myQuery($sql,$result);
		$rows = array();
		while($row = mysql_fetch_assoc($result))
		{
			$rows[] = $row;
		}
		mysql_free_result($result);
		return $rows;
	}
	
	//-- Lay ra info ID [0] [1] cua nhieu record ---
	function getDataRow($sql)
	{
		myQuery($sql,$result);
		$rows = array();
		while($row = mysql_fetch_row($result))
		{
			$rows[] = $row;
		}
		mysql_free_result($result);
		return $rows;
	}
	
	//-- Insert Record vao Database ---
	function insertData($table,$data)
	{
		foreach($data as $key => $value)
		{
			$arr[] = "`$key`='".addslashes(trim($value))."'";
		}
		$arr_value = implode(",",$arr);
		
		$sql="INSERT INTO $table SET $arr_value";
		myQuery($sql,$result);
	}
	
	//-- Update Record vao Database ---
	function updateData($table,$data,$where)
	{
		foreach($data as $key => $value)
		{
			$arr[] = "`$key` = '".addslashes(trim($value))."'";
		}
		$arr_value = implode(",",$arr);
	
		if($where != "")
		{
			$sql="UPDATE `$table` SET $arr_value WHERE $where";
			myQuery($sql,$result);
		}
		else
		{
			echo "UPDATE Command Missing WHERE Value";
		}
	}
	
	//-- Delete Record ---
	function deleteData($table,$where)
	{
		if($where != "")
		{
			$sql="DELETE FROM `$table` WHERE $where";
			myQuery($sql,$result);
		}
		else
		{
			echo "DELETE Command Missing WHERE Value";
		}
	}	

	//-- Tao lenh WHERE de su dung ---
	function getWhere($data)
	{
		if(is_array($data))
		{
			foreach($data as $key => $value)
			{
				$arr[] = "`$key` = '$value'";
			}
			$where = implode(" AND ",$arr);
		}
		else
		{
			$where=$data;
		}
		return $where;
	}


	//-- dung de phan trang 1-2-3-4-5-6 ---	 truyen cau SQL vao
	function Slit_Page($row_per_page, $table, $where=null)
	{
		// Đọan Code dùng để phân trang, trả lại 2 biến $Page[total] $Page[limit]
		// $where = " WHERE $cat_id = $_GET["cat_id"];
		// $Page	=	$DATA->Slit_Page(20,'news',$where);
		//  	for ($i=0 ; $i<$Page['total'] ; $i++) 
		//		{
		//		$No[$i]	=	$i+1;
		// }
		// $sql = "SELECT * FROM `news` ORDER BY `rank` DESC $Page[limit]";
		
		// Phần hiển thị phân trang
		//	<?php if ($i>0) { ? >
		//	<table border="0" cellpadding="5" cellspacing="0" style="border-collapse: collapse"  class="home">
		//	<tr>
		//	<td>Page</td>
		//	<?php  foreach($No as $value): ? >
		//	<?php if($value==$p) { echo "<td><b>$value</b></td>"; } 
		//	else { ? >
		//	<td><a href="index.php?mod=news&obj=news_list&p=<?php echo $value ? >"><?php echo $value; ? ></a></td>
		//	< ?php } endforeach; ? >
		//	</tr>
		//	</table>
		//	<?php } else {echo "Hiện tại chưa có Tin Tức nào được cập nhật.";} ? >
			
		isset($_GET['p'])	?	$_GET['p']=$_GET['p']	:	$_GET['p'] = 1;
	
		$Page['limit']	=	($_GET['p'] - 1) * $row_per_page;
		$Page['limit']	=	"LIMIT $Page[limit], ".$row_per_page;
		
		$sql	=	"SELECT COUNT(*) AS `count` FROM `$table` $where";
		$Page['count'] = getCount($sql);
		
		if ($Page['count']  % $row_per_page == 0) {
			$Page['total']	=	$Page['count'] 	/	$row_per_page;
		}
		else {
			$Page['total']	=	intval(($Page['count'] 	/	$row_per_page)) + 1;
		}
		return $Page;
	}
	
	//-- dung de phan trang 1-2-3-4-5-6 ---	truyen COUNT vao luon
	function Slit_Page_Search($row_per_page, $number)
	{
		isset($_GET['p'])	?	$_GET['p']=$_GET['p']	:	$_GET['p'] = 1;
	
		$Page['limit']	=	($_GET['p'] - 1) * $row_per_page;
		$Page['limit']	=	"LIMIT $Page[limit], ".$row_per_page;

		$Page['count'] = $number;
		
		if ($Page['count']  % $row_per_page == 0) {
			$Page['total']	=	$Page['count'] 	/	$row_per_page;
		}
		else {
			$Page['total']	=	intval(($Page['count'] 	/	$row_per_page)) + 1;
		}
		return $Page;
	}		

?>