<?php
//--- Chuyen website tu WWW -> NONE WWW
if(substr($_SERVER['SERVER_NAME'],0,4) === "www.")
    header('Location: '.URL_PATH.$_SERVER['REQUEST_URI']);
	
//--- Doc va xu ly URL than thien
if(strpos($_SERVER["QUERY_STRING"], '.html') !== false )
{
	$temppp = explode('.html',$_SERVER["QUERY_STRING"]); $link = $temppp[0].".html";
	$url = explode('/',$temppp[0]);
	$url_mod = @$url[0];
	$url_act = @$url[1];
	$url_opt = @$url[2];

	switch ($url_mod) {
		
		case 'tim-kiem-san-pham' :
			$_GET["mod"] = "product"; $_GET["act"] = "list"; 
			//http://asifac.com.vn/tim-kiem-bai-viet/search.html?keyword=k�nh
			break;	

		case 'danh-muc' :
			$_GET["mod"]="product";	$_GET["act"] = "list";
			$option = explode("_",$url_act);
			if(isset($option[1])) $_GET["cat_id"] = abs(intval($option[1])); else $_GET["cat_id"] = 1;
			//http://localhost/danh-muc/thuoc-bot_1.html
			break;
			
		case 'san-pham' :
			$_GET["mod"]="product";	$_GET["act"] = "detail" ;
			$option = explode("_",$url_act); 
			$option2 = explode("-",$option[1]); 
			$_GET["cat_id"] = abs(intval($option2[0]));
			$_GET["id"] = abs(intval($option2[1]));
			//http://localhost/san-pham/asi-factory_1-5.html
			break;	

		case 'tai-khoan' :
			if($url_act=="dang-nhap") {$_GET["mod"]="user"; $_GET["act"]="login";}
			elseif($url_act=="dang-ky") {$_GET["mod"]="user"; $_GET["act"]="register";}
			elseif($url_act=="kich-hoat-tai-khoan") {$_GET["mod"]="user"; $_GET["act"]="register_active";}
			elseif($url_act=="thong-tin") {$_GET["mod"]="user"; $_GET["act"]="info";}
			elseif($url_act=="lich-su-mua-hang") {$_GET["mod"]="user"; $_GET["act"]="order";}
			elseif($url_act=="chi-tiet-don-hang") {$_GET["mod"]="user"; $_GET["act"]="order_detail";}
			elseif($url_act=="danh-sach-yeu-thich") {$_GET["mod"]="user"; $_GET["act"]="wishlist";}
			elseif($url_act=="dang-xuat") {$_GET["mod"]="user"; $_GET["act"]="logout";}
			//http://localhost/tai-khoan/dang-nhap.html
			//http://localhost/thong-tin/dieu-khoan-su-dung_23.html (id=23)
			break;	
								
		case 'dat-hang' :
			if($url_act=="gio-hang") {$_GET["mod"]="order"; $_GET["act"]="cart";}
			elseif($url_act=="thong-tin-mua-hang") {$_GET["mod"]="order"; $_GET["act"]="form";}
			elseif($url_act=="xac-nhan-thong-tin") {$_GET["mod"]="order"; $_GET["act"]="confirm";}
			elseif($url_act=="hoan-tat") {$_GET["mod"]="order"; $_GET["act"]="complete";}
			//http://localhost/dat-hang/gio-hang.html
			//http://localhost/dat-hang/thong-tin-mua-hang.html
			break;
			
		case 'thong-tin' :
			$_GET["mod"]="content"; $_GET["act"]="info";
			$url_opt = explode("_",$url_act);
			$_GET["id"] = abs(intval($url_opt[1]));
			//http://localhost/thong-tin/gioi-thieu_1.html (id=1 hoac id default)
			//http://localhost/thong-tin/dieu-khoan-su-dung_23.html (id=23)
			break;
			
		case 'hien-thi' :
			if($url_act=="dang-nhap") {$_GET["mod"]="ajax"; $_GET["act"]="login";}
			elseif($url_act=="quen-mat-khau") {$_GET["mod"]="ajax"; $_GET["act"]="forgot_pass";}
			elseif($url_act=="gioi-thieu-ban-be") {$_GET["mod"]="ajax"; $_GET["act"]="send_friend";}
			elseif($url_act=="them-vao-gio-hang") {$_GET["mod"]="ajax"; $_GET["act"]="cart";}
			elseif($url_act=="them-vao-danh-sach-yeu-thich") {$_GET["mod"]="ajax"; $_GET["act"]="wishlist";}
			elseif($url_act=="danh-gia-san-pham") {$_GET["mod"]="ajax"; $_GET["act"]="review";}
			//http://localhost/hien-thi/gioi-thieu-ban-be.html
			break;				
	}
}

?>
