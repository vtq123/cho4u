<?php
	session_start();
	//error_reporting(0);

	ob_start();	
	define('SESSION_NAME','_cho4u');
	require_once('../function/dbi.php');
	require_once('../function/validation.php');	
	require_once('../function/path.php');

	if ($mod=='ajax')
	{
		require_once('module/popup/'.$act.'.php');
	}
	elseif(isset($_SESSION['Admin'.SESSION_NAME]))
	{
		$menu = "module/home/menu.php";
		$main = $dir;
		$show_menu=1;
		require_once('module/home/view/structure.tpl.php');
	}
	else
	{
		$main = "module/home/login.php";
		require_once('module/home/view/structure.tpl.php');
	}
	ob_end_flush();
?>