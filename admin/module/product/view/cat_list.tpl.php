<form name="frm" method="POST">
<tr>
	<td style="border-left:solid 1px #CCC;border-right:solid 1px #CCC;padding:10">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#fbfbfb">
						<tr height="6">
							<td width="5" background="images/s_crn_tl.png"></td>
							<td background="images/s_top.png"></td>
							<td width="6" background="images/s_crn_tr.png"></td> 
						</tr>
						<tr>
							<td width="5" background="images/s_left.png"></td>
							<td height="48" style="padding:5">
							
								<table cellpadding="0" cellspacing="0" align="left">
									<tr height="48">
										<td width="48" background="images/header/language.png"></td>
										<td class="header">Danh mục sản phẩm</td>
								  </tr>
								</table>
								
								<table cellpadding="0" cellspacing="0" align="right">
									<tr height="48">
										<td width="50" align="center">
											<table width="40" cellpadding="0" cellspacing="0" class="toolbar" onMouseOver="this.className='toolbar_hover'" onMouseOut="this.className='toolbar'" onClick="document.location.href='index.php?mod=product&act=cat_add'">
												<tr height="32">
													<td width="100%" background="images/toolbar/icon-32-new.png" class="toolbar_icon"></td>
												</tr>
												<tr>
													<td width="100%" class="toolbar_text">Thêm</td>
												</tr>
											</table>
										</td>
										<td width="50" align="center">
											<table width="40" cellpadding="0" cellspacing="0" class="toolbar" onMouseOver="this.className='toolbar_hover'" onMouseOut="this.className='toolbar'" onClick="if (confirm('Bạn chắc chắn muốn xóa những phần đã chọn ?')) document.frm.action='index.php?mod=product&act=cat_del' ; document.frm.submit();">
												<tr height="32">
													<td width="100%" background="images/toolbar/icon-32-trash.png" class="toolbar_icon"></td>
												</tr>
												<tr>
													<td width="100%" class="toolbar_text">Xóa</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								
							</td>
							<td width="6" background="images/s_right.png"></td>					
						</tr>
						<tr height="6">
							<td width="5" background="images/s_crn_bl.png"></td>
							<td background="images/s_bottom.png"></td>
							<td width="6" background="images/s_crn_br.png"></td> 
						</tr>
					</table>	
				</td>
			</tr>
			<tr>
			  <td height="5" class="home">&nbsp;</td>
		  </tr>
			<tr>
				<td>
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr height="6">
							<td width="5" background="images/s_crn_tl.png"></td>
							<td background="images/s_top.png"></td>
							<td width="6" background="images/s_crn_tr.png"></td> 
						</tr>
						<tr>
							<td width="5" background="images/s_left.png"></td>
							<td style="padding:5"><table width="100%" cellpadding="0" cellspacing="0" class="core_border">
                              <tr>
                                <td width="4%" class="core_title"><input onclick="javascript:checkall(frm.all, frm,'chk[]');" name="all" title="Chọn tất cả" type="checkbox" /></td>
                                <td width="4%" class="core_title_id">#</td>
                                <td width="46%" class="core_title">Tên Danh Mục</td>
                                <td width="16%" class="core_title">Hình ảnh</td>
                                <td class="core_title">Số sản phẩm</td>
                                <td colspan="3" class="core_title">Thứ tự <img src="images/icon/filesave.png" align="absmiddle" style="cursor:pointer" title="Cập nhật danh mục" onclick="document.frm.action='index.php?mod=product&amp;act=cat_rank'; document.frm.submit();" /></td>
                                <td width="8%" class="core_title">Hiển thị</td>
                              </tr>
                              <?php foreach($Data as $data): ?>
                              <tr class="core_row2">
                                <td class="core_value_id"><input type="checkbox" name="chk[]" value="<?php echo $data["id"];?>" /></td>
                                <td class="core_value_id"><strong><?php echo $stt;?></strong></td>
                                <td class="core_value"><a href="index.php?mod=product&amp;act=cat_edit&amp;id=<?php echo $data["id"];?>"><b style="color:#F00"><?php echo $data["name"];?></b></a></td>
                                <td class="core_value_id">
                                <?php if($data['image']!="") { ?><img src="<?php echo "../data/product_cat/".$data['image']; ?>" width="25"/><?php } ?>
                                </td>
                                <td width="10%" class="core_value_id">&nbsp;</td>
                                <td width="4%" class="core_value_id"><input name="<?php echo $data['id'] ?>" type="text" id="<?php echo $data['id'] ?>" value="<?php echo $data['rank'] ?>" style="width:50px;text-align:center;font-size:11px;" /><input type="hidden" name="id[]" value="<?php echo $data['id'] ?>" /></td>
                                <td class="core_value_id">&nbsp;</td>
                                <td class="core_value_id">&nbsp;</td>
                                <td class="core_value_id"><?php if($data['status']=='Y') {?>
                                    <a href="index.php?mod=product&amp;act=cat_status&amp;id=<?php echo $data['id'] ?>&amp;key=N"><img src="images/icon/publish_g.png" border="0" /></a>
                                    <?php } else { ?>
                                    <a href="index.php?mod=product&amp;act=cat_status&amp;id=<?php echo $data['id'] ?>&amp;key=Y"><img src="images/icon/publish_r.png" border="0" /></a>
                                    <?php } ?>
                                </td>  
                              </tr>
                              
<?php   
	$sql = "SELECT * FROM `product_cat` WHERE `cat_id`='".$data['id']."' ORDER BY `rank` ASC";
	$Data_sub = getDataInfo($sql);
	$sumsum2 = count($Data_sub);
	$stt_sub = 1;
	foreach($Data_sub as $data_sub):
?>
                              
                              <tr class="core_row3">
                                <td class="core_value_id"><input type="checkbox" name="chk[]" value="<?php echo $data["id"];?>" /></td>
                                <td class="core_value_id"></td>
                                <td class="core_value"><?php echo $stt_sub."." ?>&nbsp;&nbsp;<a href="index.php?mod=product&amp;act=cat_edit&amp;id=<?php echo $data_sub["id"];?>"><b><?php echo $data_sub["name"];?></b></a></td>
                                <td class="core_value_id">Cột <?php echo $data_sub["cat_column"]+1;?></td>
                                <td class="core_value_id">
                                
                                <a href="index.php?mod=product&amp;act=product_list&amp;cat_id=<?php echo $data_sub["id"]; ?>" style="text-decoration:none" target="_blank"><strong><?php $sql = "SELECT COUNT(*) AS `count` FROM `product` WHERE `cat_id`='".$data_sub["id"]."'"; $aaa = getCount($sql); if($aaa>0) echo $aaa; ?></strong></a>
                                
                                </td>
                        		<td class="core_value_id">&nbsp;</td>
                                <td width="4%" class="core_value_id"><input name="<?php echo $data_sub['id'] ?>" type="text" id="<?php echo $data_sub['id'] ?>" value="<?php echo $data_sub['rank'] ?>" style="width:50px;text-align:center;font-size:11px;" /><input type="hidden" name="id[]" value="<?php echo $data_sub['id'] ?>" /></td>
                                <td class="core_value_id">&nbsp;</td>
                                
                                <td class="core_value_id"><?php if($data_sub['status']=='Y') {?>
                                    <a href="index.php?mod=product&amp;act=cat_status&amp;id=<?php echo $data_sub['id'] ?>&amp;key=N"><img src="images/icon/publish_g.png" border="0" /></a>
                                    <?php } else { ?>
                                    <a href="index.php?mod=product&amp;act=cat_status&amp;id=<?php echo $data_sub['id'] ?>&amp;key=Y"><img src="images/icon/publish_r.png" border="0" /></a>
                                    <?php } ?>
                                </td> 
                              </tr>

	<?php   
        $sql = "SELECT * FROM `product_cat` WHERE `cat_id`='".$data_sub['id']."' ORDER BY `rank` ASC";
        $Data_sub_sub = getDataInfo($sql);
        $sumsum3 = count($Data_sub_sub);
        foreach($Data_sub_sub as $data_sub_sub):
    ?>
                              <tr class="core_row">
                                <td width="4%" class="core_value_id"><input type="checkbox" name="chk[]" value="<?php echo $data_sub_sub["id"];?>" /></td>
                                <td width="4%" class="core_value_id">&nbsp;</td>
                                <td width="46%" class="core_value">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo "|---"; ?>&nbsp;&nbsp;<a href="index.php?mod=product&amp;act=cat_edit&amp;id=<?php echo $data_sub_sub["id"];?>"><?php echo $data_sub_sub["name"];?></a></td>
                                <td class="core_value_id"></td>
                                <td class="core_value_id"><a href="index.php?mod=product&act=product_list&cat_id=<?php echo $data_sub_sub["id"]; ?>"><strong><?php $sql = "SELECT COUNT(*) AS `count` FROM `product` WHERE `cat_id`='".$data_sub_sub["id"]."'"; $aaa = getCount($sql); if($aaa>0) echo $aaa; ?></strong></a></td>
                  				<td class="core_value_id">
								
                                </td>                                
                                <td class="core_value_id"></td>
                                <td width="4%" class="core_value_id"><input name="<?php echo $data_sub_sub['id'] ?>" type="text" id="<?php echo $data_sub_sub['id'] ?>" value="<?php echo $data_sub_sub['rank'] ?>" style="width:50px;text-align:center;font-size:11px;" /><input type="hidden" name="id[]" value="<?php echo $data_sub_sub['id'] ?>" /></td>
                                <td width="8%" class="core_value_id"><?php if($data_sub_sub['status']=='Y') {?>
                                    <a href="index.php?mod=product&amp;act=cat_status&amp;id=<?php echo $data_sub_sub['id'] ?>&amp;key=N"><img src="images/icon/publish_g.png" border="0" /></a>
                                    <?php } else { ?>
                                    <a href="index.php?mod=product&amp;act=cat_status&amp;id=<?php echo $data_sub_sub['id'] ?>&amp;key=Y"><img src="images/icon/publish_r.png" border="0" /></a>
                                    <?php } ?></td>
                              </tr>
<?php  endforeach; ?>


<?php $stt_sub++; endforeach; ?>

<?php $stt++; endforeach; ?>
</table></td>
							<td width="6" background="images/s_right.png"></td>					
						</tr>
						<tr height="6">
							<td width="5" background="images/s_crn_bl.png"></td>
							<td background="images/s_bottom.png"></td>
							<td width="6" background="images/s_crn_br.png"></td> 
						</tr>
					</table>	
			  </td>
			</tr>
		</table>
	
	</td>
</tr>
</form>