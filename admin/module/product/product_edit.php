<?php

	if (isset($_POST['name'])) // insert new record or edit record
	{
		if($_POST["id"]==0) // insert new record
		{
			if(isset($_POST['new'])) $new = "Y"; else $new = "N";
	
			$price 	= str_replace(",", "", $_POST["price"]);
			$price_original = str_replace(",", "", $_POST["price_original"]);
			$price_down_phan_tram = str_replace(",", "", $_POST["price_down_phan_tram"]);
			$detail = str_replace("../data/", "data/", $_POST["detail"]);		
	
			$data = array(
				"cat_id" 		=> $_POST["cat_id"],		
				"new" 			=> $new,
						
				"name" 			=> $_POST["name"],
				"code" 			=> $_POST["code"],
				"unit" 			=> $_POST["unit"],
				"model"			=> $_POST["model"],	
				
				"size" 			=> $_POST["size2"]."<<<>>>".$_POST["size"],
				"color" 		=> $_POST["color2"]."<<<>>>".$_POST["color"],
				"weight" 		=> $_POST["weight2"]."<<<>>>".$_POST["weight"],
				"pin" 			=> $_POST["pin2"]."<<<>>>".$_POST["pin"],
				"material" 		=> $_POST["material2"]."<<<>>>".$_POST["material"],
				"xuatxu" 		=> $_POST["xuatxu2"]."<<<>>>".$_POST["xuatxu"],
				"nhasanxuat" 	=> $_POST["nhasanxuat2"]."<<<>>>".$_POST["nhasanxuat"],
				"baohanh" 		=> $_POST["baohanh2"]."<<<>>>".$_POST["baohanh"],
				
				"price"					=> $price,
				"price_original"		=> $price_original,
				"price_down_phan_tram"	=> $price_down_phan_tram,
				"short_detail" 			=> $_POST["short_detail"],
				"tinhnangnoibat" 		=> $_POST["tinhnangnoibat"],
				"detail" 				=> $detail,
				
				"author" 			=> $_SESSION['User'],
				"meta_description" 	=> $_POST["meta_description"],
				"meta_keywords" 	=> $_POST["meta_keywords"],
				"meta_title" 		=> $_POST["meta_title"],					
				"posted_date" 		=> date("Y-m-d")
			);
			insertData("product",$data);
	
			//--- lay ID cua san pham moi nhat
			$sql = "SELECT `id` FROM `product` ORDER BY `id` DESC LIMIT 1";
			$id_info = getData($sql);
			$id = $id_info["id"];
	
			//--- add rank 
			$values = "`rank`='$id'";
			
			//--- add hinh anh				
			require_once("../function/image.php");
			$IMG = new IMAGE_UPLOAD_RESIZE();	
			$prefix = strip_url($_POST["name"])."-cho4u-".$id;
	
			if(isset($_FILES['pic']) && !empty($_FILES['pic']['tmp_name'])){
				$IMG->IMG_Save('../data/product/',$prefix,'pic');
				$IMG->IMG_Resize_Width(180,$IMG->url,'../data/product/'.$prefix,'pic');
				$pic = str_replace("../data/product/", "", $IMG->url);
				$values = "`pic`='$pic'";
			}
			if(isset($_FILES['pic_1']) && !empty($_FILES['pic_1']['tmp_name'])){
				$IMG->IMG_Save('../data/product/pic_1/',$prefix,'pic_1');
				$IMG->IMG_Resize_Width(400,$IMG->url,'../data/product/pic_1/'.$prefix,'pic_1');
				$pic_1 = str_replace("../data/product/pic_1/", "", $IMG->url);
				$values .= ",`pic_1`='$pic_1'";
			}
			if(isset($_FILES['pic_2']) && !empty($_FILES['pic_2']['tmp_name'])){
				$IMG->IMG_Save('../data/product/pic_2/',$prefix,'pic_2');
				$IMG->IMG_Resize_Width(400,$IMG->url,'../data/product/pic_2/'.$prefix,'pic_2');
				$pic_2 = str_replace("../data/product/pic_2/", "", $IMG->url);
				$values .= ",`pic_2`='$pic_2'";
			}		
			if(isset($_FILES['pic_3']) && !empty($_FILES['pic_3']['tmp_name'])){
				$IMG->IMG_Save('../data/product/pic_3/',$prefix,'pic_3');
				$IMG->IMG_Resize_Width(400,$IMG->url,'../data/product/pic_3/'.$prefix,'pic_3');
				$pic_3 = str_replace("../data/product/pic_3/", "", $IMG->url);
				$values .= ",`pic_3`='$pic_3'";
			}
			if(isset($_FILES['pic_4']) && !empty($_FILES['pic_4']['tmp_name'])){
				$IMG->IMG_Save('../data/product/pic_4/',$prefix,'pic_4');
				$IMG->IMG_Resize_Width(400,$IMG->url,'../data/product/pic_4/'.$prefix,'pic_4');
				$pic_4 = str_replace("../data/product/pic_4/", "", $IMG->url);
				$values .= ",`pic_4`='$pic_4'";
			}

			$sql = "UPDATE `product` SET $values WHERE `id`='$id'" ;
			myQuery($sql,$result);		
					
		}
		else // edit record
		{
			$id = $_POST["id"];
			
			if(isset($_POST['new'])) $new = "Y"; else $new = "N";
	
			$price 	= str_replace(",", "", $_POST["price"]);
			$price_original = str_replace(",", "", $_POST["price_original"]);
			$price_down_phan_tram = str_replace(",", "", $_POST["price_down_phan_tram"]);
			$detail = str_replace("../data/", "data/", $_POST["detail"]);	
			
			$data = array(
				"cat_id" 		=> $_POST["cat_id"],		
				"new" 			=> $new,
				
				"name" 			=> $_POST["name"],
				"code" 			=> $_POST["code"],
				"unit" 			=> $_POST["unit"],
				"model"			=> $_POST["model"],
				
				"size" 			=> $_POST["size2"]."<<<>>>".$_POST["size"],
				"color" 		=> $_POST["color2"]."<<<>>>".$_POST["color"],
				"weight" 		=> $_POST["weight2"]."<<<>>>".$_POST["weight"],
				"pin" 			=> $_POST["pin2"]."<<<>>>".$_POST["pin"],
				"material" 		=> $_POST["material2"]."<<<>>>".$_POST["material"],
				"xuatxu" 		=> $_POST["xuatxu2"]."<<<>>>".$_POST["xuatxu"],
				"nhasanxuat" 	=> $_POST["nhasanxuat2"]."<<<>>>".$_POST["nhasanxuat"],
				"baohanh" 		=> $_POST["baohanh2"]."<<<>>>".$_POST["baohanh"],
			
				"price"					=> $price,
				"price_original"		=> $price_original,
				"price_down_phan_tram"	=> $price_down_phan_tram,
				"short_detail" 			=> $_POST["short_detail"],
				"tinhnangnoibat" 		=> $_POST["tinhnangnoibat"],
				"detail" 				=> $detail,
				
				"views" 			=> $_POST["views"],
				"views_last_date" 	=> $_POST["views_last_date"],
				"author" 			=> $_SESSION['User'],
				"meta_description" 	=> $_POST["meta_description"],
				"meta_keywords" 	=> $_POST["meta_keywords"],
				"meta_title" 		=> $_POST["meta_title"],
				"posted_date" 		=> date("Y-m-d")				
			);
			$where = "`id`='$id'";
			updateData("product",$data,$where);	

			$values = "`id`='$id'";
			require_once("../function/image.php");
			$IMG = new IMAGE_UPLOAD_RESIZE();	
			$prefix = strip_url($_POST["name"])."-cho4u-".$id;
			
			if(isset($_FILES['pic']) && !empty($_FILES['pic']['tmp_name'])){
				$IMG->IMG_Save('../data/product/',$prefix,'pic');
				$IMG->IMG_Resize_Width(180,$IMG->url,'../data/product/'.$prefix,'pic');
				$pic = str_replace("../data/product/", "", $IMG->url);
				$values .= ",`pic`='$pic'";
			}
			if(isset($_FILES['pic_1']) && !empty($_FILES['pic_1']['tmp_name'])){
				$IMG->IMG_Save('../data/product/pic_1/',$prefix,'pic_1');
				$IMG->IMG_Resize_Width(400,$IMG->url,'../data/product/pic_1/'.$prefix,'pic_1');
				$pic_1 = str_replace("../data/product/pic_1/", "", $IMG->url);
				$values .= ",`pic_1`='$pic_1'";
			}
			if(isset($_FILES['pic_2']) && !empty($_FILES['pic_2']['tmp_name'])){
				$IMG->IMG_Save('../data/product/pic_2/',$prefix,'pic_2');
				$IMG->IMG_Resize_Width(400,$IMG->url,'../data/product/pic_2/'.$prefix,'pic_2');
				$pic_2 = str_replace("../data/product/pic_2/", "", $IMG->url);
				$values .= ",`pic_2`='$pic_2'";
			}		
			if(isset($_FILES['pic_3']) && !empty($_FILES['pic_3']['tmp_name'])){
				$IMG->IMG_Save('../data/product/pic_3/',$prefix,'pic_3');
				$IMG->IMG_Resize_Width(400,$IMG->url,'../data/product/pic_3/'.$prefix,'pic_3');
				$pic_3 = str_replace("../data/product/pic_3/", "", $IMG->url);
				$values .= ",`pic_3`='$pic_3'";
			}
			if(isset($_FILES['pic_4']) && !empty($_FILES['pic_4']['tmp_name'])){
				$IMG->IMG_Save('../data/product/pic_4/',$prefix,'pic_4');
				$IMG->IMG_Resize_Width(400,$IMG->url,'../data/product/pic_4/'.$prefix,'pic_4');
				$pic_4 = str_replace("../data/product/pic_4/", "", $IMG->url);
				$values .= ",`pic_4`='$pic_4'";
			}
			
			if($values != "`id`='$id'")
			{
				$sql = "UPDATE `product` SET $values WHERE `id`='$id'" ;
				myQuery($sql,$result);
			}
		}
			
		header("Location: ".$_SESSION['admin_redirect']);
		
		
	}
	else // read record information
	{
		isset($_GET['id'])	?	$id = $_GET['id']	:	$id = 0;
		if($id==0) $title = "Thêm mới"; else $title = "Chỉnh sửa";
		$cat_list = array(); $stt = 1;
		
		// lay info cua san pham
		$sql = "SELECT * FROM `product` WHERE `id` = '$id'";
		$data = getData($sql);
		
		$size = explode("<<<>>>",$data["size"]);
		$color = explode("<<<>>>",$data["color"]);
		$weight = explode("<<<>>>",$data["weight"]);
		$pin = explode("<<<>>>",$data["pin"]);
		$material = explode("<<<>>>",$data["material"]);
		$xuatxu = explode("<<<>>>",$data["xuatxu"]);
		$nhasanxuat = explode("<<<>>>",$data["nhasanxuat"]);
		$baohanh = explode("<<<>>>",$data["baohanh"]);

		// lay info cua Cat
		$sql = "SELECT `name`,`id` FROM `product_cat` WHERE `cat_id`=0 ORDER BY `rank` ASC,`id` DESC";
		$Cat = getDataInfo($sql);
	
		require_once('module/product/view/product_edit.tpl.php');
	}

?>