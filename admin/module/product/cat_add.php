<?php
	if (isset($_POST['name']))
	{	
		$data = array(
			"cat_id" 		=> $_POST["cat_id"],
			"name" 			=> $_POST["name"],
			"width" 		=> $_POST["width"],
			"height" 		=> $_POST["height"],
			"cat_column" 	=> $_POST["cat_column"],
			"posted_date" 	=> date("Y-m-d"),
			"meta_description" 	=> $_POST["meta_description"],
			"meta_keywords" 	=> $_POST["meta_keywords"],
			"meta_title" 		=> $_POST["meta_title"],			
			"author" 		=> $_SESSION['User']
		);
		insertData("product_cat",$data) ;
		
		$sql = "SELECT `id` FROM `product_cat` ORDER BY `id` DESC LIMIT 1";
		$id = getData($sql);
				
		$sql = "UPDATE `product_cat` SET `rank`='".$id["id"]."' WHERE `id`='".$id["id"]."'" ;
		myQuery($sql,$result);
		
		require_once('../function/image.php');
		$IMG = new IMAGE_UPLOAD_RESIZE();
		if(isset($_FILES['image']) && !empty($_FILES['image']['tmp_name'])){
			$prefix = strip_url($_POST["name"])."-cho4u-".$id["id"];
			$IMG->IMG_Save('../data/product_cat/',$prefix,'image');
			$pic = $IMG->url;
			$pic_link = str_replace("../data/product_cat/", "", $pic);
			$sql = "UPDATE `product_cat` SET `image`='$pic_link' WHERE `id`='".$id["id"]."'" ;
			myQuery($sql,$result);
		}	
		if(isset($_FILES['image_bg']) && !empty($_FILES['image_bg']['tmp_name'])){
			$prefix = strip_url($_POST["image_bg"])."-bg-cho4u-".$id["id"];
			$IMG->IMG_Save('../data/product_cat/',$prefix,'image_bg');
			$pic = $IMG->url;
			$pic_link = str_replace("../data/product_cat/", "", $pic);
			$sql = "UPDATE `product_cat` SET `image_bg`='$pic_link' WHERE `id`='".$id["id"]."'" ;
			myQuery($sql,$result);
		}		
		
		header("Location: ".$_SESSION['admin_redirect']);
	}
	else
	{
		$sql = "SELECT * FROM `product_cat` WHERE `cat_id`='0' ORDER BY `rank`";
		$Cat = getDataInfo($sql);
		
		require_once('module/product/view/cat_add.tpl.php');
	}
?>