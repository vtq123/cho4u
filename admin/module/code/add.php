<?php

	if (isset($_POST['code']))
	{
		$data = array(
			"user_id" 		=> $_POST["user_id"],
			"code" 			=> $_POST["code"],
			"price_down" 	=> str_replace(".","",$_POST["price_down"]),
			"price_limit" 	=> str_replace(".","",$_POST["price_limit"]),
			"use_status" 	=> $_POST["use_status"],
			"use_email" 	=> $_POST["use_email"],
			"use_order" 	=> $_POST["use_order"],
			"use_date" 		=> $_POST["use_date"],
			"status" 		=> $_POST["status"],
			"start_date" 	=> $_POST["start_date"],
			"stop_date" 	=> $_POST["stop_date"],
			
			"author" 		=> $_SESSION['User'],
			"posted_date" 	=> date("Y-m-d")
		);
		insertData("code",$data) ;

		header("Location: ".$_SESSION['admin_redirect']);
	}
	else
	{
		require_once('module/code/view/add.tpl.php');
	}
?>