<tr>
	<td style="border-left:solid 1px #CCC;border-right:solid 1px #CCC;padding:10">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#fbfbfb">
						<tr height="6">
							<td width="5" background="images/s_crn_tl.png"></td>
							<td background="images/s_top.png"></td>
							<td width="6" background="images/s_crn_tr.png"></td> 
						</tr>
						<tr>
							<td width="5" background="images/s_left.png"></td>
							<td height="48" style="padding:5">
							
								<table cellpadding="0" cellspacing="0" align="left">
									<tr height="48">
										<td width="48" background="images/header/article.png"></td>
										<td class="header">Mã Giảm Giá</td>
								  </tr>
								</table>
                                
                                <table cellpadding="0" cellspacing="0" align="right">
									<tr height="48">
										<td width="50" align="center">
											<table width="40" cellpadding="0" cellspacing="0" class="toolbar" onMouseOver="this.className='toolbar_hover'" onMouseOut="this.className='toolbar'" onClick="document.location.href='index.php?mod=code&act=add'">
												<tr height="32">
													<td width="100%" background="images/toolbar/icon-32-new.png" class="toolbar_icon"></td>
												</tr>
												<tr>
													<td width="100%" class="toolbar_text">Thêm</td>
												</tr>
											</table>
										</td>
										<td width="50" align="center">
											<table width="40" cellpadding="0" cellspacing="0" class="toolbar" onMouseOver="this.className='toolbar_hover'" onMouseOut="this.className='toolbar'" onClick="if (confirm('Bạn chắc chắn muốn xóa những phần đã chọn ?')) document.frm.action='index.php?mod=code&act=del' ; document.frm.submit();">
												<tr height="32">
													<td width="100%" background="images/toolbar/icon-32-trash.png" class="toolbar_icon"></td>
												</tr>
												<tr>
													<td width="100%" class="toolbar_text">Xóa</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
								
								</td>
							<td width="6" background="images/s_right.png"></td>					
						</tr>
						<tr height="6">
							<td width="5" background="images/s_crn_bl.png"></td>
							<td background="images/s_bottom.png"></td>
							<td width="6" background="images/s_crn_br.png"></td> 
						</tr>
					</table>	

				</td>
			</tr>
			<tr>
              <td height="30" align="right">
              
              <form id="search" method="GET" name="search" action="index.php" style="margin:0">
                <input name="mod" type="hidden" value="code" />
                <input name="act" type="hidden" value="list" />

                <select name="search_status">
                    <option value="0">Xem tất cả Mã Khuyến Mãi</option>
                    <option value="1" <?php if($search_status==1) echo " selected"?>>|-- Mã chưa sử dụng</option> 
                    <option value="2" <?php if($search_status==2) echo " selected"?>>|-- Mã đã sử dụng</option>                    
               </select>

                <input name="search_name" type="text" id="search_name" value="<?php echo $search_name; ?>" style="width:200px;font-size:11px;" placeholder="Tìm Mã KM / User ID / User Email"/>
                
                <input type="submit"  name="submit" value="Tìm kiếm" style="font-size:11px;"/>
                <input type="button"  name="reset" value="Reset" id="reset" onclick="document.location.href='index.php?mod=code&act=list'" style="font-size:11px;"/>
             </form>

              </td>
            </tr>
			<tr>
				<td>
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr height="6">
							<td width="5" background="images/s_crn_tl.png"></td>
							<td background="images/s_top.png"></td>
							<td width="6" background="images/s_crn_tr.png"></td> 
						</tr>
						<tr>
							<td width="5" background="images/s_left.png"></td>
							<td style="padding:5">

							
<form name="frm" method="POST" style="margin:0">								
								<table width="100%" cellpadding="0" cellspacing="0" class="core_border">
									<tr>
									  <td width="2%" class="core_title_id"><input onclick="javascript:checkall(frm.all, frm,'chk[]');" name="all" title="Chọn tất cả" type="checkbox" /></td>
									  <td width="10%" align="left" class="core_title">Mã Code</td>
									  <td width="6%" align="left" class="core_title">Set ID</td>
									  <td width="29%" align="left" class="core_title">Số tiền giảm</td>
									  <td width="9%" align="left" class="core_title">Hạn sử dụng</td>
									  <td width="5%" class="core_title">Status</td>
									  <td width="8%" class="core_title">Ngày tạo</td>
									  <td width="7%" class="core_title">Order</td>
									  <td width="13%" class="core_title">Người sử dụng</td>
									  <td width="11%" class="core_title">Ngày sử dụng</td>
								  </tr>
<?php foreach($Data as $data): ?>
					                <tr class="core_row" onmouseover="this.className='core_row_over'" onmouseout="this.className='core_row'">
					                  <td class="core_value_id"><input type="checkbox" name="chk[]" value="<?php echo $data['id'] ?>" /></td>
					                  <td class="core_value_2"><a href="index.php?mod=code&act=edit&id=<?php echo $data['id'] ?>"><strong style="color:#F00;font-size:14px"><?php echo stripslashes($data['code']); ?></strong></a></td>
					                  <td class="core_value_id"><?php if ($data['user_id']>0) echo $data['user_id']; else echo "all"; ?></td>
					                  <td class="core_value"><strong style="color:#F00;font-size:14px"><?php echo number_format($data['price_down']); ?></strong> <?php if ($data['price_limit']>0) echo "order tối thiểu ".number_format($data['price_limit']); ?></td>
					                  <td class="core_value_id"><?php echo $data['start_date']."<br>".$data['stop_date']; ?></td>
					                  <td class="core_value_id"><?php if($data['status']=='Y') {?>
                                          <a href="index.php?mod=code&amp;act=status&amp;id=<?php echo $data['id'] ?>&amp;key=N"><img src="images/icon/publish_g.png" border="0" /></a>
                                          <?php } else { ?>
                                          <a href="index.php?mod=code&amp;act=status&amp;id=<?php echo $data['id'] ?>&amp;key=Y"><img src="images/icon/publish_r.png" border="0" /></a>
                                          <?php } ?></td>
					                  <td class="core_value_id"><?php echo $data['posted_date']; ?></td>
					                  <td class="core_value_id"><?php if($data['use_order']>0) echo $data['use_order']; ?></td>
					                  <td class="core_value_id"><?php echo $data['use_email']; ?></td>
					                  <td class="core_value_id"><?php echo $data['use_date']; ?></td>
				                  </tr>
<?php endforeach; ?>                                   
								</table>
</form>
</td>
							<td width="6" background="images/s_right.png"></td>					
						</tr>
						<tr height="6">
							<td width="5" background="images/s_crn_bl.png"></td>
							<td background="images/s_bottom.png"></td>
							<td width="6" background="images/s_crn_br.png"></td> 
						</tr>
					</table>	

				</td>
			</tr>
		</table>
	
	</td>
</tr>
