<?php
	//--- Count All ----
	$sql = "SELECT COUNT(*) AS `count` FROM `product`";
	$sum_all = getCount($sql);
	$sql = "SELECT COUNT(DISTINCT `product_id`) AS `count` FROM `product_to_cat`,`product`
				WHERE `product`.`id` = `product_to_cat`.`product_id`";
	$sum_online = getCount($sql);
	
	$sql = "SELECT COUNT(*) AS `count` FROM `product` WHERE `new`='Y'";
	$sum_new = getCount($sql);
	$sql = "SELECT COUNT(*) AS `count` FROM `product` WHERE `hot`='Y'";
	$sum_hot = getCount($sql);	
	$sql = "SELECT COUNT(*) AS `count` FROM `product` WHERE `only`='Y'";
	$sum_only = getCount($sql);		
	$sql = "SELECT COUNT(*) AS `count` FROM `product` WHERE `price_original` > `price`";
	$sum_price_down = getCount($sql);
	$sql = "SELECT COUNT(*) AS `count` FROM `product` WHERE `thanhly`='Y'";
	$sum_thanhly = getCount($sql);
	$sql = "SELECT COUNT(*) AS `count` FROM `product` WHERE `status`='Y'";
	$sum_status = getCount($sql);
	
	$sql = "SELECT COUNT(*) AS `count` FROM `review`"; 
	$sum_review = getCount($sql);
	$sql = "SELECT COUNT(*) AS `count` FROM `review` WHERE `status`='N'"; 
	$sum_review_hide = getCount($sql);
	$sql = "SELECT COUNT(*) AS `count` FROM `wishlist`"; 
	$sum_wishlist = getCount($sql);

	$sql = "SELECT COUNT(*) AS `count` FROM `user`"; 
	$sum_user = getCount($sql);
	$sql = "SELECT COUNT(DISTINCT `user_id`) AS `count` FROM `order`"; 
	$sum_user_order = getCount($sql);		
	
	$sql = "SELECT COUNT(*) AS `count` FROM `order`"; 
	$sum_order = getCount($sql);
	$sql = "SELECT COUNT(*) AS `count` FROM `order` WHERE `status`='0'"; 
	$sum_order_0 = getCount($sql);
	$sql = "SELECT COUNT(*) AS `count` FROM `order` WHERE `status`='1'"; 
	$sum_order_1 = getCount($sql);
	$sql = "SELECT COUNT(*) AS `count` FROM `order` WHERE `status`='2'"; 
	$sum_order_2 = getCount($sql);	
	$sum_order_3 = 	$sum_order - $sum_order_0 - $sum_order_1 - $sum_order_2;		

	require_once('module/thongke/view/list.tpl.php');
?>