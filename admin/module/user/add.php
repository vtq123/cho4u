<?php
	
	if (isset($_POST['user']))
	{
		$message[] = check_email($_POST['user'],"User Email không hợp lệ, vui lòng nhập lại.");
		$message[] = check_empty($_POST['pass'],"Vui lòng nhập Password vào Form.");
		$message[] = check_value_ready("user","`user` = '".$_POST["user"]."'","Tài khoản này đã có, vui lòng nhập lại.");
		check_alert($message);

		$data = array(
			"user" 				=> $_POST["user"],
			"pass" 				=> md5($_POST["pass"]),
			"register_date" 	=> date("Y-m-d"),
			"register_ip" 		=> "Admin đăng ký dùm",
			"email_active" 		=> $_POST["email_active"],
			"email_code" 		=> $_POST["email_code"],
			"block" 			=> $_POST["block"],
			"block_reason"		=> $_POST["block_reason"],

			"name" 				=> $_POST["name"],
			//"sex" 				=> $_POST["sex"],
			"birthday" 			=> date_to_mysql($_POST["birthday"]),
			"address" 			=> $_POST["address"],
			"city" 				=> $_POST["city"],
			//"country" 			=> $_POST["country"],
			"phone" 			=> $_POST["phone"],

			"author" 			=> $_SESSION['User'],
			"posted_date"		=> date("Y-m-d")		
		);
		insertData("user",$data);

		header("Location: ".$_SESSION['admin_redirect']);
	}
	
	// Lay info Tinh Thanh
	$sql = "SELECT `id`,`name` FROM `cat_thanhpho` ORDER BY `name` ASC,`id` DESC";
	$Tinhthanh = getDataInfo($sql);	

	require_once('module/user/view/add.tpl.php');
	
?>