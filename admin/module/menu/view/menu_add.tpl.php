<form name="frm" method="post" enctype="multipart/form-data">
<input type="hidden" name="action" value="true" />
<tr>
	<td style="border-left:solid 1px #CCC;border-right:solid 1px #CCC;padding:10">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#fbfbfb">
						<tr height="6">
							<td width="5" background="images/s_crn_tl.png"></td>
							<td background="images/s_top.png"></td>
							<td width="6" background="images/s_crn_tr.png"></td> 
						</tr>
						<tr>
							<td width="5" background="images/s_left.png"></td>
							<td height="48" style="padding:5">
							
								<table cellpadding="0" cellspacing="0" align="left">
									<tr height="48">
										<td width="48" background="images/header/article.png"></td>
										<td class="header">Danh mục &gt; Thêm mới</td>
								  </tr>
								</table>
								
								<table cellpadding="0" cellspacing="0" align="right">
									<tr height="48">
										<td width="50" align="center">
											<table width="45" cellpadding="0" cellspacing="0" class="toolbar" onMouseOver="this.className='toolbar_hover'" onMouseOut="this.className='toolbar'" onclick="history.back()">
												<tr height="32">
													<td width="100%" background="images/toolbar/icon-32-cancel.png" class="toolbar_icon"></td>
												</tr>
												<tr>
													<td width="100%" class="toolbar_text">Quay lại</td>
												</tr>
											</table>
										</td>
										<td width="50" align="center">
											<table width="40" cellpadding="0" cellspacing="0" class="toolbar" onMouseOver="this.className='toolbar_hover'" onMouseOut="this.className='toolbar'" onclick="frm.submit();">
												<tr height="32">
													<td width="100%" background="images/toolbar/icon-32-save.png" class="toolbar_icon"></td>
												</tr>
												<tr>
													<td width="100%" class="toolbar_text">Lưu</td>
											  </tr>
											</table>
										</td>
									</tr>
								</table>
								
							</td>
							<td width="6" background="images/s_right.png"></td>					
						</tr>
						<tr height="6">
							<td width="5" background="images/s_crn_bl.png"></td>
							<td background="images/s_bottom.png"></td>
							<td width="6" background="images/s_crn_br.png"></td> 
						</tr>
					</table>	
				</td>
			</tr>
			<tr><td height="10"></td></tr>
			<tr>
				<td>
					<table width="100%" cellpadding="0" cellspacing="0">
						<tr height="6">
							<td width="5" background="images/s_crn_tl.png"></td>
							<td background="images/s_top.png"></td>
							<td width="6" background="images/s_crn_tr.png"></td> 
						</tr>
						<tr>
							<td width="5" background="images/s_left.png"></td>
							<td style="padding:7; padding-left:17; padding-right:17">
								<table width="100%" cellpadding="0" cellspacing="0">
									<tr>
										<td>										  <br />
											<table width="70%" align="center" cellpadding="0" cellspacing="0">
				  <tr>
													<td>				
														<table width="100%" cellpadding="0" cellspacing="0">
															<tr height="7">
																<td width="7"></td>
																<td rowspan="2" class="detail_title">Thêm mới</td>
															  <td width="80%"></td>
																<td width="7"></td>
															</tr>
															<tr height="7">
																<td background="images/s_crn_tl_r.png"></td>										
																<td background="images/s_crn_t_r.png"></td>										
																<td background="images/s_crn_tr_r.png"></td>
															</tr>			
														</table>													</td>
												</tr>
												<tr>
													<td style="padding:7; border-left:solid 1px #CCCCCC; border-right:solid 1px #CCCCCC; border-bottom:solid 1px #CCCCCC"><table width="100%" cellpadding="0" cellspacing="0">
											  <tr>
											    <td class="detail_left">&nbsp;</td>
											    <td class="detail_right">&nbsp;</td>
											    </tr>
											  <tr>
																<td width="23%" class="detail_left">Tên danh mục</td>
											    <td width="77%" class="detail_right"><input name="name" type="text" id="name" value="" size="45" /></td>
														</tr>
															<tr>
															  <td class="detail_left">Nằm trong Danh mục</td>
															  <td class="detail_right">
															  <select name="cat_id" id="cat_id">
															  <option value="0">Danh Mục Gốc</option>
 <?php  foreach($Cat as $cat): ?>
<option value="<?php echo $cat["id"] ?>" <?php if ($cat_id == $cat["id"]) echo "selected=selected"; ?>> + <?php echo $cat["name"] ?></option>
<?php endforeach; ?>
                                                              </select>
															  
															  </td>
													  </tr>
															<tr>
															  <td class="detail_left">Url</td>
															  <td class="detail_right"><input name="url" type="text" id="url" value="" size="60" /></td>
													  </tr>
															<tr>
															  <td class="detail_left">Image</td>
															  <td class="detail_right"><input name="image" type="text" id="image" value="" size="45" /></td>
													  </tr>
															<tr>
                                                              <td class="detail_left">Hiển thị</td>
															  <td class="detail_right"><input name="status" type="radio" id="radio" value="Y" checked="checked" />
															    Có
															    <input type="radio" name="status" id="radio2" value="N" />
															    Không</td>
													  </tr>
															<tr>
															  <td class="detail_left">&nbsp;</td>
															  <td class="detail_right">&nbsp;</td>
													  </tr>								
												  </table>												  </td>
												</tr>
											</table>									  
											<br /></td>
								  </tr>
							</table>	
							</td>
							<td width="6" background="images/s_right.png"></td>					
						</tr>
						<tr height="6">
							<td width="5" background="images/s_crn_bl.png"></td>
							<td background="images/s_bottom.png"></td>
							<td width="6" background="images/s_crn_br.png"></td> 
						</tr>
					</table>	
				</td>
			</tr>
		</table>
	
	</td>
</tr>
</form>