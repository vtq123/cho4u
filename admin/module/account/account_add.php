<?php

	if (isset($_POST['user'])) 
	{
		if (isset($_POST['per']) && is_array($_POST['per'])) {
			foreach ($_POST['per'] as $key => $value) {
				$perm[$key] = $value;			
			}
		}
		
		$permission = implode('-',$perm);
		
		$data = array(
			"user" 			=> $_POST["user"],
			"pass" 			=> md5($_POST['pass']),
			"permission" 	=> $permission,
			"status" 		=> $_POST["status"],
			"posted_date" 	=> date("Y-m-d"),
			"author" 		=> $_SESSION['User']
		);
		insertData("admin_account",$data) ;
		header("Location: ".$_SESSION['admin_redirect']);
	}
	else  
	{
		$sql = "SELECT * FROM `admin_menu` WHERE `cat_id`='0' AND `status`='Y' ORDER BY `rank`";
		$Menu =	getDataInfo($sql);

		require_once('module/account/view/account_add.tpl.php');
	}

?>