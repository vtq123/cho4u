<form name="frm" method="post" style="margin:0" enctype="multipart/form-data">
<input type="hidden" name="id" value="<?php echo $id ?>" />
<tr>
	<td style="border-left:solid 1px #CCC;border-right:solid 1px #CCC;padding:10">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<table width="100%" cellpadding="0" cellspacing="0" bgcolor="#fbfbfb">
						<tr height="6">
							<td width="5" background="images/s_crn_tl.png"></td>
							<td background="images/s_top.png"></td>
							<td width="6" background="images/s_crn_tr.png"></td> 
						</tr>
						<tr>
							<td width="5" background="images/s_left.png"></td>
							<td height="48" style="padding:5"><table cellpadding="0" cellspacing="0" align="left">
									<tr height="48">
										<td width="48" background="images/header/article.png"></td>
										<td class="header">Cập nhật thông tin đơn hàng</td>
							  </tr>
								</table>
								
								<table cellpadding="0" cellspacing="0" align="right">
									<tr height="48">
										<td width="50" align="center">
											<table width="45" cellpadding="0" cellspacing="0" class="toolbar" onMouseOver="this.className='toolbar_hover'" onMouseOut="this.className='toolbar'" onclick="document.location.href='<?php echo $_SESSION['admin_redirect'] ?>'">
												<tr height="32">
													<td width="100%" background="images/toolbar/icon-32-back.png" class="toolbar_icon"></td>
												</tr>
												<tr>
													<td width="100%" class="toolbar_text">Trở lại</td>
											  </tr>
											</table>										</td>
										<td width="50" align="center">
											<table width="40" cellpadding="0" cellspacing="0" class="toolbar" onMouseOver="this.className='toolbar_hover'" onMouseOut="this.className='toolbar'" >
												<tr height="32">
													<td width="100%" align="center" class="toolbar_icon"><input src="images/toolbar/icon-32-save.png" name="submit" type="image" border="0"/></td>
											  </tr>
												<tr>
													<td width="100%" class="toolbar_text">L&#432;u</td>
											  </tr>
											</table>										</td>
									</tr>
								</table>							</td>
						  <td width="6" background="images/s_right.png"></td>					
						</tr>
						<tr height="6">
							<td width="5" background="images/s_crn_bl.png"></td>
							<td background="images/s_bottom.png"></td>
							<td width="6" background="images/s_crn_br.png"></td> 
						</tr>
					</table>	

			  </td>
			</tr>
			<tr><td height="10"></td></tr>
			<tr>
				<td>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr height="6">
                        <td width="5" background="images/s_crn_tl.png"></td>
                        <td background="images/s_top.png"></td>
                        <td width="6" background="images/s_crn_tr.png"></td> 
                    </tr>
                    <tr>
                        <td width="5" background="images/s_left.png"></td>
                        <td style="padding:7; padding-left:17; padding-right:17">
                        <table width="100%" align="center" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="99" valign="top" class="detail_left"><font color="#FF0000"><strong><br />
                            Người mua</strong></font></td>
                            <td width="432" valign="top" class="detail_right">
                            <strong><br />
                            <?php echo $user["name"]; ?></strong><br /><?php echo $user["address"]." - ".$user["city"]; ?><br /><?php echo $user["phone"]; ?><br /><br /></td>
                            <td width="110" valign="top" class="detail_left"><strong><font color="#FF0000"><br />
                            Mã đơn hàng</font></strong></td>
                            <td width="324" valign="top" class="detail_right"><strong><br />
                            <?php echo $user["id"] ?></strong><br />
                              <select name="status" id="status" style="width:110px;">
                                <option value="0" <?php if($user["status"]==0) echo 'selected="selected"'?> >Đơn hàng mới</option>
                                <option  value="1" <?php if($user["status"]==1) echo 'selected="selected"'?> >Đang xử lý</option>
                                <option value="2" <?php if($user["status"]==2) echo 'selected="selected"'?> >Đã hoàn tất</option>
                                <option value="3" <?php if($user["status"]==3) echo 'selected="selected"'?> >Đơn hàng hủy</option>
                              </select>
                              &nbsp;<input type="image" src="images/icon/write.gif" style="width:15px;height:15px;padding-top:10px;"/>                               </td>
     						</tr>
                            <tr>
                              <td valign="top" class="detail_left"><font color="#FF0000"><strong>Người nhận</strong></font></td>
                              <td valign="top" class="detail_right"><strong><?php if($user["name_to"]!="")echo $user["name_to"]; else echo $user["name"];?></strong><br /><?php if($user["address_to"]!="") echo $user["address_to"]; else echo $user["address"] ?><br /><?php if($user["phone_to"]!="") echo $user["phone_to"]; else echo $user["phone"]; ?><br /><br /></td>
                              <td valign="top" class="detail_left"><font color="#FF0000"><strong>Thanh toán</strong><br /></font></td>
                              <td valign="top" class="detail_right">
                              <select name="payment_type" id="payment_type">
                                <option value="1" <?php if($user["payment_type"]==1) echo 'selected="selected"'?> >Tiền mặt khi giao hàng</option>
                                <option  value="2" <?php if($user["payment_type"]==2) echo 'selected="selected"'?> >Chuyển khoản</option>
                                <option value="3" <?php if($user["payment_type"]==3) echo 'selected="selected"'?> >Credit Card</option>
                                <option value="4" <?php if($user["payment_type"]==4) echo 'selected="selected"'?> >Paypal</option>
                                <option value="5" <?php if($user["payment_type"]==5) echo 'selected="selected"'?> >Ngân lượng</option>
                              </select><br />
                              <?php
                                if($user["payment_type"]==1) echo ""; 
                                elseif($user["payment_type"]==2) echo $user["ck_info"]; 
                                elseif($user["payment_type"]==3) echo "Type: ".$user["cc_type"]."<br />Owner: ".$user["cc_owner"]."<br />Number: ".$user["cc_number"]." - Expire: ".$user["cc_expires"]; 
                                elseif($user["payment_type"]==4) echo "";
                                elseif($user["payment_type"]==5) echo ""; 
                              ?>
                              </td>
      						</tr>
                            <tr>
                              <td colspan="4">&nbsp;</td>
                            </tr>
                            <tr>
                              <td colspan="4">
                              
                              <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="color:#1e557b;font-family:Tahoma;font-size:11px;">
                                  <tr bgcolor="#b9d1e4">
                                    <td width="3%" height="30" align="center">&nbsp;</td>
                                    <td width="16%" align="center"><strong>Sản phẩm</strong></td>
                                    <td width="41%" align="center"><strong>Mô tả</strong></td>
                                    <td width="13%" align="center"><strong>Đơn giá</strong></td>
                                    <td width="12%" align="center"><strong>Số lượng</strong></td>
                                    <td width="15%" align="right"><div style="padding-right:20px;"><strong>Thành tiền</strong></div></td>
                                </tr>
<?php
foreach($Items as $product):
$price_total = $product["price"]*$product["quantity"];
$total = $total + $price_total;
?>
<input type="hidden" name="num[]" value="<?php echo $product["id"] ?>" />              
                                  <tr <?php if($stt%2==0) echo "style='background:#eff4f9;'";?>>
                                    <td height="80" align="right"><strong><?php echo $stt ?>.</strong></td>
                                    <td align="center"><div style="width:70px;height:70px;overflow:hidden;border:1px solid #91b7d4;"><img src="../data/product/<?php echo $product["pic"] ?>" width="70" height="70" style="padding:2px;"/></div></td>
                                    <td style="line-height:20px;">
                                    <?php if($product["code"]!="") { ?><strong><?php echo $product["code"] ?></strong><br /><?php } ?><strong style="color:#FF0000;font-size:12px"><?php echo $product["name"] ?></strong><br />
                                    <input name="detail_<?php echo $product["id"] ?>" type="text" id="detail_<?php echo $product["id"] ?>" style="width:250px;border:1px solid #DDDDDD;padding:2px;font-size:11px;color:#666666" value="<?php echo $product["detail"]; ?>" size="1" autocomplete="off" onFocus="show_div('show_detail_<?php echo $product["id"] ?>');"/><span id="show_detail_<?php echo $product["id"] ?>" style="display:none;"><input type="submit" name="submit" id="submit" value="cập nhật" style="font-size:11px;width:60px;"/></span></td>
                                    <td align="center">
                                    <div><input name="price_original_<?php echo $product["id"] ?>" type="text" id="price_original_<?php echo $product["id"] ?>" style="width:100px;font-size:12px;text-align:right;border:1px solid #DDDDDD;padding:2px;text-decoration:line-through;color:#666666"  value="<?php echo number_format($product["price_original"]); ?>" size="1" autocomplete="off" onFocus="show_div('show_price_<?php echo $product["id"] ?>');" /></div>
                                    <div><input name="price_<?php echo $product["id"] ?>" type="text" id="price_<?php echo $product["id"] ?>" style="width:100px;font-size:12px;text-align:right;border:1px solid #DDDDDD;padding:2px;font-weight:bold;color:#0000FF"  value="<?php echo number_format($product["price"]); ?>" size="1" autocomplete="off" onFocus="show_div('show_price_<?php echo $product["id"] ?>');" /></div>
                                    <div style="display:none;" id="show_price_<?php echo $product["id"] ?>"><input type="submit" name="submit" id="submit" value="cập nhật" style="font-size:11px;width:60px;"/></div>
                                    </td>
                                    <td align="center">
                                    <div><input name="quantity_<?php echo $product["id"] ?>" type="text" id="quantity_<?php echo $product["id"] ?>" style="width:38px;text-align:center;color:#FF0000;font-size:12px;border:1px solid #DDDDDD;padding:2px"  value="<?php echo $product["quantity"] ?>" size="1" maxlength="3" autocomplete="off" onFocus="show_div('show_quantity_<?php echo $product["id"] ?>');"/></div>
                                    <div style="display:none;" id="show_quantity_<?php echo $product["id"] ?>"><input type="submit" name="submit" id="submit" value="cập nhật" style="font-size:11px;width:60px;"/></div>
                                    
                                    </td>
                                    <td align="right" style="padding-right:20px;"><strong><?php echo number_format($price_total); ?></strong></td>
                                </tr>
                                  <tr>
                                    <td colspan="6" bgcolor="#b9d1e4" height="1"></td>
                                  </tr>
<?php $stt++; endforeach; ?>                                   
                                  
                                 <tr>
                                   <td height="30" colspan="3">* Hình minh họa có thể bị thay đổi hoặc bị xóa.</td>
                                   <td>&nbsp;</td>
                                   <td align="right"><strong>Phí giao hàng</strong></td>
                                   <td align="right">
                                   <div style="float:right;width:20px;display:none" id="shipping_fee_update"><input type="image" src="images/icon/write.gif" width="15" height="15" title="Cập nhật" /></div>
                                   <div style="float:right;width:20px;" id="shipping_fee_update_2">&nbsp;</div>
                                   <div style="float:right;width:100px;"><input name="shipping_fee" type="text" id="shipping_fee" style="width:100px;text-align:right;font-weight:bold;color:#FF0000;font-size:11px;border:1px solid #DDDDDD;padding:2px" value="<?php echo number_format($user["shipping_fee"])?>" maxlength="8" onclick="show_div('shipping_fee_update');hide_div('shipping_fee_update_2');"/></div>                                                           </td>
                                 </tr>
                                  <tr>
                                    <td colspan="6" bgcolor="#b9d1e4" height="1"></td>
                                  </tr>
                                  <tr>
                                    <td height="30" colspan="3">&nbsp;</td>
                                    <td align="right">
                                    <div <?php if($user["promotion_code"]=="") echo 'style="display:none"'; ?> id="promotion_code_update_1"><input name="promotion_code" type="text" id="promotion_code" style="width:100%;text-align:center;font-size:11px;border:1px solid #CCCCCC" onblur="if (this.value=='') this.value='Nhập mã giảm giá';" onfocus="if (this.value=='Nhập mã giảm giá') this.value='';" value="<?php if($user["promotion_code"]!="") echo $user["promotion_code"]; else echo "Nhập mã giảm giá"; ?>" maxlength="8" /></div></td>
                                    <td align="right"><strong>Phiếu giảm giá</strong></td>
                                    <td align="right">
                                    <div style="float:right;width:20px;display:none" id="promotion_code_update"><input type="image" src="images/icon/write.gif" width="15" height="15" title="Cập nhật" /></div>
                                   <div style="float:right;width:20px;" id="promotion_code_update_2">&nbsp;</div>
                                   <div style="float:right;width:100px;"><input name="promotion_code_fee" type="text" id="promotion_code_fee" style="width:100px;text-align:right;font-size:11px;border:1px solid #DDDDDD;padding:2px" value="<?php echo number_format($user["promotion_code_fee"])?>" maxlength="8" onclick="show_div('promotion_code_update');show_div('promotion_code_update_1');hide_div('promotion_code_update_2');"/></div>                                    </td>
                                  </tr>
                                  <tr>
                                    <td colspan="6" bgcolor="#b9d1e4" height="1"></td>                                                         
                                 <tr bgcolor="#b9d1e4">
                                    <td height="30" colspan="3">&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td align="right"><strong>TỔNG CỘNG</strong></td>
                                    <td align="right"><div style="padding-right:20px;"><strong><?php echo number_format($total + $user["shipping_fee"] - $user["promotion_code_fee"]) ?></strong></div></td>
                                </tr>
                                <tr>
                                  <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td align="center">&nbsp;</td>
                                    <td align="right">&nbsp;</td>
                                </tr>
                                  <tr>
                                    <td height="60" colspan="6"><textarea name="note" id="note" style="width:100%;height:100px;font-family:Arial;font-size:12px;padding:3px;"><?php if($user["note"]!="")echo $user["note"]; else echo "Ghi chú:"; ?></textarea></td>
                                  </tr>
                                  <tr>
                                    <td height="60" colspan="6" align="center">
                                    <input type="submit" name="submit" id="submit" value="Cập nhật thông tin" />
                                    &nbsp;&nbsp;&nbsp;<input type="button" name="button" id="button" value="Trở lại trang danh sách" onclick="document.location.href='<?php echo $_SESSION['admin_redirect'] ?>'" /></td>
                                </tr>
                              </table>          											</td>
                            </tr>
                            <tr>
                              <td colspan="4">&nbsp;</td>
                            </tr>
                            <tr>
                              <td class="detail_left">&nbsp;</td>
                              <td colspan="3" class="detail_right"><label></label></td>
                            </tr>
                      </table>
                      </td>
                      <td width="6" background="images/s_right.png"></td>					
                    </tr>
                    <tr height="6">
                        <td width="5" background="images/s_crn_bl.png"></td>
                        <td background="images/s_bottom.png"></td>
                        <td width="6" background="images/s_crn_br.png"></td> 
                    </tr>
                </table>	

			  </td>
			</tr>
		</table>
	
	</td>
</tr>
</form>