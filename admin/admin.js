function show_bar(id) {
	if (document.getElementById(id).style.display == 'none') 
		document.getElementById(id).style.display = ''
	else
		document.getElementById(id).style.display = 'none'	
}

function checkall(obj , frm , name) {
	
	for (var i=0 ; i<frm.elements.length ; i++) {
		var e = frm.elements[i];
		if ((e.name != obj.name) && (e.type == 'checkbox') && (e.name == name)) {
			if (e.disabled == false) {
				e.checked = obj.checked;
			}			
		}		
	}
	
}

function setCheckedAll_with_name(obj, fr, name) {
	for (var i = 0; i < fr.elements.length; i++) 
	{
		var e = fr.elements[i];
		if ((e.name != obj.name) && (e.type=='checkbox') && (e.name==name)) {			
			if (e.disabled==false)
				e.checked = obj.checked;			
		}
	}
}