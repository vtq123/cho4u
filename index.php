<?php
	session_start();
	//error_reporting(0);

	//define('URL_PATH','http://theseus-s.com/');
	define('URL_PATH','http://localhost/code/');
	define('URL_NAME','CHO4U');
	define('SESSION_NAME','_cho4u');

	ob_start();	
	require_once('function/dbi.php');
	require_once('function/validation.php');	
	require_once('function/url_friendly.php');
	require_once('function/path.php');

		
	if ($mod=='ajax')
	{
		require_once('module/popup/'.$act.'.php');
	}
	else
	{
		require_once('module/home/setting.php');
		$header = "module/home/header.php";
		$main = $dir;
		$footer = "module/home/footer.php";
		require_once('module/home/view/structure.tpl.php');
	}
	ob_end_flush();
?>