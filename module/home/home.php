<?php

	// Cac Product moi cap nhat
	$sql = "SELECT `id`,`cat_id`,`name`,`price`,`price_original`,`pic`
			FROM `product` WHERE `status`='Y' AND `new`='Y'
			ORDER BY `rank` DESC,`id` DESC LIMIT 40";
	$Product = getDataInfo($sql);
	
	// Banner Slide
	$sql = "SELECT * FROM `banner` WHERE `status`='Y' ORDER BY `rank` ASC,`id` ASC";
	$Banner = getDataInfo($sql);
	
	// Banner Left
	$sql = "SELECT * FROM `banner_left` WHERE `status`='Y' ORDER BY `rank` ASC,`id` ASC LIMIT 2";
	$Banner_left = getDataInfo($sql);	
	
	// Brand
	$sql = "SELECT * FROM `brand` WHERE `status`='Y' ORDER BY `rank` ASC,`id` ASC";
	$Brand = getDataInfo($sql);	
	
	require_once('module/home/view/home.tpl.php'); 
?>