<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $web_info["meta_title"]; ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="Description" content="<?php echo $web_info["meta_description"]; ?>" />
<meta name="Keywords" content="<?php echo $web_info["meta_keywords"]; ?>" />
<base href="<?php echo URL_PATH ?>" />
<link href="user.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="libs/fancybox/fancybox.css" media="screen" />
<script type="text/javascript" src="libs/jquery/jquery_min.js"></script>	
<script type="text/javascript" src="libs/fancybox/fancybox.js"></script>
<script type="text/javascript">
function hide_div(obj){obj1=document.getElementById(obj);obj1.style.display='none';}
function show_div(obj){obj1=document.getElementById(obj);obj1.style.display='block';}
function displayRow(obj){var row=document.getElementById(obj);if(row.style.display=='')row.style.display='none';else row.style.display='';}	
$(document).ready(function(){
	$(".popup_window").fancybox({
		'titleShow'			: false,
		'autoScale'			: false,
		'padding'			: 0,
		'transitionIn'		: 'none',
		'transitionOut'		: 'none'
	});

});  
</script>
</head>

<body>
    <div id="header"><?php require_once($header); ?></div>
    <div id="content"><?php require_once($main); ?></div>
    <div id="footer"><?php require_once($footer); ?></div>
</body>
</html>