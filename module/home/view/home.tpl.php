<script type="text/javascript" src="libs/jquery/jquery.carouFredSel-4.5.1-packed.js"></script>
<script language="javascript">
$(function(){	
	$('#header_slide').carouFredSel({			
		items : 1,
		scroll: {
	       items:1,
		   duration: 800
	    },
		auto: true,
		prev : {
				button      : ".btn_left",
				key         : "left",
				items       : 1
				},
		next : {
				button      : ".btn_right",
				key         : "right",
				items       : 1
				}
	});
});
</script>

<div style="float:right;width:615px;height:322px;padding-top:30px" onmouseover="show_div('slide_icon_left');show_div('slide_icon_right');" onmouseout="hide_div('slide_icon_left');hide_div('slide_icon_right');">

    <div style="position:absolute;z-index:999;margin-top:130px;margin-left:10px;display:none;" id="slide_icon_left"><a href="javascript:void(0)" class="btn_left"><img src="images/btn_left.png" width="40" height="40" title="Chuyển Slide"/></a></div>
    <div style="position:absolute;z-index:999;margin-top:130px;margin-left:562px;display:none" id="slide_icon_right"><a href="javascript:void(0)" class="btn_right"><img src="images/btn_right.png" width="40" height="40" title="Chuyển Slide"/></a></div>

    <div class="header_slide_home">
          <ul id="header_slide">
            <?php foreach($Banner as $banner) { ?>
            <li><a href="<?php echo $banner["url"]; ?>"  <?php if($banner["target"]=="Y") echo 'target="_blank"'; ?>><img src="data/banner/<?php echo $banner["image"]; ?>" border="0" width="615" height="322"/></a></li>
            <?php } ?>
          </ul>
    </div>
</div>

<div style="clear:both;height:25px"></div>

<div style="width:940px;height:40px;line-height:40px;background:url(images/bg-title.gif);"><strong style="font-size:14px;padding-left:50px">SẢN PHẨM BÁN CHẠY</strong><img src="images/button-xemtatca.gif" width="89" height="26" alt="View All" style="float:right;padding:7px;display:none" /></div>

<div style="clear:both;height:25px"></div>

<div style="clear:both;text-align:center">

<?php foreach($Product as $product) { ?>   
    <div style="float:left;width:25%;height:325px;text-align:center" >
    <?php if($product["price_original"]>$product["price"]) { ?><div style="position:absolute;padding-left:23px"><img src="images/sale-label-product.png" width="55" height="55" alt="sale off" /></div><?php } ?>
    	<a href="san-pham/<?php echo strip_url($product["name"])."_".$product["cat_id"]."-".$product["id"]; ?>.html"><img src="<?php if($product["pic"]!="") echo "data/product/".$product["pic"]; else echo "images/shopbigsize-image-default.jpg"; ?>" width="180" height="180" style="padding:5px" /></a>
    	<br /><span style="font-size:12px;" class="text_black"><?php echo $product["name"]; ?></span>
        <div style="height:28px;padding-top:5px"><?php if($product["price_original"]>$product["price"]) { ?><span style="text-decoration:line-through;color:#390"><?php echo number_format($product["price_original"]); ?></span>&nbsp;&nbsp;&nbsp;<?php } ?><span class="text_red" style="font-size:16px"><?php echo number_format($product["price"]); ?> vnđ</span></div>
        
        <a class="popup_window" href="<?php if(isset($_SESSION['user'.SESSION_NAME])) echo "hien-thi/them-vao-danh-sach-yeu-thich.html?id=".$product["id"]; else echo "hien-thi/dang-nhap.html"; ?>"><img src="images/button-wishlist.gif" width="35" height="35" alt="wishlist" onmouseout="src='images/button-wishlist.gif'" onmouseover="src='images/button-wishlist-over.gif'" /></a>
        <a href="san-pham/<?php echo strip_url($product["name"])."_".$product["cat_id"]."-".$product["id"]; ?>.html"><img src="images/button-order.gif" width="77" height="35" alt="order"onmouseout="src='images/button-order.gif'" onmouseover="src='images/button-order-over.gif'" /></a>
    </div>
    
    
<?php } ?>    
    
</div>

<div style="clear:both;height:25px"></div>

<div>
<a href="<?php echo $Banner_left[0]["url"]; ?>"  <?php if($Banner_left[0]["target"]=="Y") echo 'target="_blank"'; ?>><img src="data/banner_left/<?php echo $Banner_left[0]["image"]; ?>" width="455" height="160" style="float:left" /></a>
<a href="<?php echo $Banner_left[1]["url"]; ?>"  <?php if($Banner_left[1]["target"]=="Y") echo 'target="_blank"'; ?>><img src="data/banner_left/<?php echo $Banner_left[1]["image"]; ?>" width="455" height="160" style="float:right" /></a>
</div>

<div style="clear:both;height:25px"></div>

<?php if(count($Brand) > 0) { ?>
<script language="javascript">
$(function(){	
	$('#brand_slide').carouFredSel({
		items 	: 5,
		prev	: '#prev',
		next	: '#next',
		scroll	: {
		   items		: 1,
		   duration		: 800,
		   fx			: "directscroll",
		   direction 	: "up",
		   pauseOnHover	: true
		},
		auto	: true
	});	
});
</script>

<div style="width:940px;height:40px;line-height:40px;background:url(images/bg-title.gif);"><strong style="font-size:14px;padding-left:50px">ĐỐI TÁC CỦA CHÚNG TÔI</strong></div>

<div style="clear:both;height:25px"></div>

<div class="brand_slide">

	<div style="float:left;width:35px;"><img src="images/back_over.gif" width="22" height="53" vspace="10" style="cursor:pointer" id="prev" onmouseover="src='images/back_orange.gif'" onmouseout="src='images/back_over.gif'" /></div>
    
	<div style="float:left;text-align:center" class="brand_content">
        <ul id="brand_slide">
        <?php foreach($Brand as $data) { ?>
            <li><img src="data/brand/<?php echo $data["image"] ?>" width="140" /></li>
        <?php } ?>
        </ul>
    </div>
    
    <div style="float:left"><img src="images/next_over.gif" width="22" height="53" vspace="10" style="cursor:pointer" id="next" onmouseover="src='images/next_orange.gif'" onmouseout="src='images/next_over.gif'" /></div>

</div>
<?php } ?>

<div style="clear:both;height:50px"></div>
