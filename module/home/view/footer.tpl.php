<div style="width:100%;background-color:#eeeeee">
	<div style="width:940px;margin-left:auto;margin-right:auto;padding:30px 0">
		<div style="float:left;width:25%;line-height:22px">
        	<strong style="font-size:16px">Giới thiệu về Cho4u</strong>
            <?php foreach($Column_01 as $data) { ?>
            <br /><a href="thong-tin/<?php echo strip_url($data["name"])."_".$data["id"]; ?>.html"><?php echo $data["name"]; ?></a>
            <?php } ?>
	  </div>
        
        <div style="float:left;width:25%;line-height:22px">
        	<strong style="font-size:16px">Đặt hàng và vận chuyển</strong>
            <?php foreach($Column_02 as $data) { ?>
            <br /><a href="thong-tin/<?php echo strip_url($data["name"])."_".$data["id"]; ?>.html"><?php echo $data["name"]; ?></a>
            <?php } ?>
	  </div>
        
        <div style="float:left;width:25%;line-height:22px">
       	  	<strong style="font-size:16px">Quy định và chính sách</strong>
          	<?php foreach($Column_03 as $data) { ?>
            <br /><a href="thong-tin/<?php echo strip_url($data["name"])."_".$data["id"]; ?>.html"><?php echo $data["name"]; ?></a>
            <?php } ?>
	  </div>
        
        <div style="float:left;width:25%;line-height:22px">
        	<strong style="font-size:16px">Thông tin cần biết</strong>
            <?php foreach($Column_04 as $data) { ?>
            <br /><a href="thong-tin/<?php echo strip_url($data["name"])."_".$data["id"]; ?>.html"><?php echo $data["name"]; ?></a>
            <?php } ?>
	  </div>
        
        <div style="clear:both"></div>
    </div>
    
    <div style="clear:both;height:1px;background-color:#cccccc"></div>
</div>

<div style="width:100%;background-color:#dddddd">
	<div style="width:940px;margin-left:auto;margin-right:auto;padding:30px 0">
		<div style="float:left;width:20%;line-height:22px;text-align:center"><img src="images/icon-footer-01.gif" width="54" height="54" /><br />
		  <strong style="font-size:14px">Chất lượng - Đa dạng</strong><br />
		  <span>Sản phẩm chất lượng cao<br />cùng mẫu mã đa dạng<br />dễ dàng chọn lựa</span></div>
        
      <div style="float:left;width:20%;line-height:22px;text-align:center"><img src="images/icon-footer-02.gif" width="54" height="54" /><br /><strong style="font-size:14px">Hỗ trợ nhiệt tình</strong><br />Hỗ trợ tư vấn nhiệt tình<br />với bất cứ thắc mắc nào<br />của khách hàng</div>
        
      <div style="float:left;width:20%;line-height:22px;text-align:center"><img src="images/icon-footer-03.gif" width="54" height="54" /><br /><strong style="font-size:14px">Giao hàng nhanh chóng</strong><br />
          Giao hàng nhanh chóng<br />
          tại nhà khách hàng trên<br />
        toàn quốc từ 3-5 ngày </div>
        
      <div style="float:left;width:20%;line-height:22px;text-align:center"><img src="images/icon-footer-04.gif" width="54" height="54" /><br /><strong style="font-size:14px">Giá cả hợp lý</strong><br />
          Giá cả cạnh tranh<br />
          hợp lý đối với nhiều<br />
        loại khách hàng</div>
        
      <div style="float:left;width:20%;line-height:22px;text-align:center"><img src="images/icon-footer-05.gif" width="54" height="54" /><br /><strong style="font-size:14px">Thanh toán an toàn</strong><br />Các phương thức thanh toán<br />an toàn, tiện lợi, đảm bảo<br />cho khách hàng khi mua sắm</div>

        <div style="clear:both"></div>
    </div>
    
    <div style="clear:both;height:1px;background-color:#cccccc"></div>
</div>


<div style="width:100%;background-color:#eeeeee;">
	<div style="width:940px;margin-left:auto;margin-right:auto;padding:20px 0">
    	<div style="float:left">&copy; Bản quyền thuộc về Chợ For You - Cho4u.vn</div>
        <div style="float:right"><img src="images/creditcard.gif" width="183" height="22" /></div>
        
        <div style="clear:both"></div>
    </div>
</div>

<style type='text/css'>
#bttop{position:fixed;bottom:35px;right:35px;cursor:pointer;display:none;}
</style>
<script type='text/javascript'>$(function(){$(window).scroll(function(){if($(this).scrollTop()!=0){$('#bttop').fadeIn();}else{$('#bttop').fadeOut();}});$('#bttop').click(function(){$('body,html').animate({scrollTop:0},800);});});</script>
<div id='bttop'><img src="images/icon_top.png" onmouseout="src='images/icon_top.png'" onmouseover="src='images/icon_top_over.png'"/></div>


<div id="bottom_bar_left" style="position:fixed;top:20px;left:20px;"> 
    <a href="<?php echo $Banner_doc[0]["url"]; ?>"  <?php if($Banner_doc[0]["target"]=="Y") echo 'target="_blank"'; ?>><img src="data/banner_doc/<?php echo $Banner_doc[0]["image"]; ?>" width="140" /></a>
</div>

<div id="bottom_bar_right" style="position:fixed;top:20px;right:20px;"> 
    <a href="<?php echo $Banner_doc[1]["url"]; ?>"  <?php if($Banner_doc[1]["target"]=="Y") echo 'target="_blank"'; ?>><img src="data/banner_doc/<?php echo $Banner_doc[1]["image"]; ?>" width="140" /></a>
</div>

<script type='text/javascript'>
function check_window_size(){
	if (typeof window.innerWidth != 'undefined')
		viewportwidth = window.innerWidth;
	else
		viewportwidth = document.documentElement.clientWidth;
  
	if(viewportwidth<1300) {
		document.getElementById('bottom_bar_left').style.display='none';
		document.getElementById('bottom_bar_right').style.display='none';
	} else {
		document.getElementById('bottom_bar_left').style.display='block';
		document.getElementById('bottom_bar_right').style.display='block';
	}
}
check_window_size();
window.onresize = function() {
	check_window_size();
}
</script>