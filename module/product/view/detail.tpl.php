<script type="text/javascript">

function order_quantity_up(){
	if(document.getElementById('order_number').value!= 99)
		document.getElementById('order_number').value=Number(document.getElementById('order_number').value) + 1;
}

function order_quantity_down(){
	if(document.getElementById('order_number').value!= 1)
		document.getElementById('order_number').value=Number(document.getElementById('order_number').value) - 1;
}

function frmAddCart(){

	if(document.getElementById('order_number').value==''){
		alert('Vui lòng nhập số lượng của sản phẩm.');
		document.getElementById('order_number').value=="1";
		document.getElementById('order_number').focus();
		return false;
	}
	
	var aaa = document.getElementById('order_number').value;
	var bbb = document.getElementById('detail').value;

	$.ajax({
		type	: "POST",
		cache	: false,
		url		: "index.php?mod=ajax&act=cart&action=add&id=<?php echo $id ?>&number="+aaa+"&detail="+bbb,
		data	: "",
		success : function(data) {
			$.fancybox(data,{
				'titleShow'			: false,
				'autoScale'			: false,
				'padding'			: 0
			});
		}
	});
	return false;
}

function change_pic(pic){
	document.getElementById('pic_main').src = pic;
}
</script>


<div style="clear:both;height:45px;line-height:45px;border-bottom:1px solid #dddddd;font-size:14px">
&nbsp;&nbsp;Trang chủ&nbsp;&nbsp;>&nbsp;&nbsp;<?php echo @$title_product_1 ?>&nbsp;&nbsp;>&nbsp;&nbsp;<?php echo @$title_product_2 ?>&nbsp;&nbsp;>&nbsp;&nbsp;<span class="text_red"><?php echo @$title_product_3 ?></span>
</div>

<div style="clear:both;height:45px"></div>

<form name="frm">

<div style="float:left;width:402px;padding-right:30px">

	<div style="width:400px;height:400px;display:table-cell;vertical-align:middle;text-align:center;display:inline-block\9;border:1px solid #d3d3d3;">
   	  <img src="<?php if($data["pic_1"]!="") echo "data/product/pic_1/".$data["pic_1"]; else echo "data/sanpham_default_400.jpg"; ?>" style="max-width:400px;height:400px;" alt="<?php echo $data["name"]; ?>" id="pic_main"/>
    </div>

	
    <div style="clear:both;width:402px;height:95px;padding-top:9px">
        <div style="float:left;margin-right:8px">
        <?php if($data["pic_1"]!="") { ?>
            <img src="data/product/pic_1/<?php echo $data["pic_1"]; ?>" style="width:92px;height:92px;border:1px solid #cccccc;cursor:pointer;" onclick="change_pic('data/product/pic_1/<?php echo $data["pic_1"]; ?>')" onmouseover="this.style.borderColor='#ff6000'" onmouseout="this.style.borderColor='#cccccc'"/>
        <?php } else { ?>
            <img src="data/sanpham_default_180.gif" style="width:93px;height:92px;border:1px solid #cccccc;"/>
        <?php } ?>
        </div>
        <div style="float:left;margin-right:8px">
        <?php if($data["pic_2"]!="") { ?>
            <img src="data/product/pic_2/<?php echo $data["pic_2"]; ?>" style="width:92px;height:92px;border:1px solid #cccccc;cursor:pointer;" onclick="change_pic('data/product/pic_2/<?php echo $data["pic_2"]; ?>')" onmouseover="this.style.borderColor='#ff6000'" onmouseout="this.style.borderColor='#cccccc'"/>
        <?php } else { ?>
            <img src="data/sanpham_default_180.gif" style="width:93px;height:92px;border:1px solid #cccccc;"/>
        <?php } ?>
        </div>
        <div style="float:left;margin-right:8px">
        <?php if($data["pic_3"]!="") { ?>
            <img src="data/product/pic_3/<?php echo $data["pic_3"]; ?>" style="width:92px;height:92px;border:1px solid #cccccc;cursor:pointer;" onclick="change_pic('data/product/pic_3/<?php echo $data["pic_3"]; ?>')" onmouseover="this.style.borderColor='#ff6000'" onmouseout="this.style.borderColor='#cccccc'"/>
        <?php } else { ?>
            <img src="data/sanpham_default_180.gif" style="width:92px;height:92px;border:1px solid #cccccc;"/>
        <?php } ?>
        </div>
        <div style="float:left;">
        <?php if($data["pic_4"]!="") { ?>
            <img src="data/product/pic_4/<?php echo $data["pic_4"]; ?>" style="width:92px;height:92px;border:1px solid #cccccc;cursor:pointer;" onclick="change_pic('data/product/pic_4/<?php echo $data["pic_4"]; ?>')" onmouseover="this.style.borderColor='#ff6000'" onmouseout="this.style.borderColor='#cccccc'"/>
        <?php } else { ?>
            <img src="data/sanpham_default_180.gif" style="width:92px;height:92px;border:1px solid #cccccc;"/>
        <?php } ?>
        </div>
    </div>    
    
</div>

<div style="float:left;width:508px;">
  <div style="height:246px;font-size:14px">
        <div style="font-size:20px;color:#F00"><strong><?php echo $data["name"]; ?></strong></div>
        <div style="height:45px;line-height:40px;color:#03F;">Mã sản phẩm: <?php echo $data["code"]; ?></div>
        <div><?php echo nl2br(stripslashes($data["short_detail"])); ?><br /><br /></div>
        <div style="color:#F00;">
            <?php if($data["price"]>0) { ?><span style="font-size:22px;"><?php echo number_format($data["price"]); ?></span> vnd / <?php echo $data["unit"]; ?><?php if($data["price_original"]>$data["price"]) { ?>&nbsp;&nbsp;&nbsp;&nbsp;<span style="color:#339900;text-decoration:line-through;"><?php echo number_format($data["price_original"]); ?></span><?php } ?><?php } else { ?><strong>Giá xin liên hệ</strong><?php } ?>
        </div>
    </div>
    
    <div>
    	Khách mua dặn dò <strong style="color:#0066FF">(size, màu, kiểu...)</strong><br />
      <textarea name="detail" id="detail" style="width:350px;height:50px;margin:10px 0"></textarea>

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="95" background="images/add_to_cart_1.gif">
                <table border="0" align="center" cellpadding="0" cellspacing="0" style="padding-left:5px">
                  <tr>
                    <td><img src="images/add_to_cart_2.gif" width="17" height="26" onclick="order_quantity_down()"/></td>
                    <td style="background-color:#FFFFFF"><input name="order_number" type="text" id="order_number" style="width:27px;color:#f24118;text-align:center;border:none;font-weight:bold;" value="1" maxlength="2"/></td>
                    <td><img src="images/add_to_cart_3.gif" width="17" height="26" onclick="order_quantity_up();"/></td>
                  </tr>
                </table></td>
                <td width="107"><img src="images/add_to_cart_4.gif" width="107" height="62" style="cursor:pointer" onClick="frmAddCart();"/></td>
                <td style="padding-left:10px">
                <div style="width:210px;height:62px;background-image:url(images/order_phone.gif);"><div style="padding:21px 0px 0px 60px;font-size:16px;color:#F00"><?php echo $web_info["phone_support"] ?></div></div>
                </td>
              </tr>
            </table>
	</div>
    
    <div style="padding-top:9px">
    	<a class="popup_window" href="<?php if(isset($_SESSION['user'.SESSION_NAME])) echo "hien-thi/them-vao-danh-sach-yeu-thich.html?id=".$id; else echo "hien-thi/dang-nhap.html"; ?>"><img src="images/add_wishlist.gif" width="196" height="30" style="padding-bottom:8px"/></a><br />
    </div>
</div>



<div style="clear:both;font-size:14px">

	<div style="padding:30px 0;"><?php echo stripslashes($data["tinhnangnoibat"]); ?></div>

	<table width="100%" cellpadding="10" cellspacing="0" style="border:1px solid #cccccc;font-size:12px">
      <tr>
        <td width="150" style="font-size:12px;color:#FFFFFF;font-weight:bold;background-color:#646464">Thông tin sản phẩm</td>
        <td width="600" style="background-color:#646464">&nbsp;</td>
      </tr>
    <?php foreach($thongtin as $ttin) { if($ttin[1]!="") { ?>  
      <tr>
        <td align="left" valign="top" style="border-top:1px solid #cccccc"><?php echo $ttin[0] ?></td>
        <td align="left" valign="top" style="border-top:1px solid #cccccc"><?php echo $ttin[1] ?></td>
      </tr>
    <?php } } ?>  
</table>

	<div style="padding:30px 0;">
		<?php echo stripslashes($data["detail"]); ?>
        <div style="text-align:right"><a href="javascript:history.back();">Trở lại trang trước</a>&nbsp;&nbsp;<img src="images/icon_danhmuc.gif" /></div>
    </div>

</div>
</form>

<div style="width:940px;height:40px;line-height:40px;background:url(images/bg-title.gif);margin:25px 0"><strong style="font-size:14px;padding-left:50px">CÓ THỂ BẠN QUAN TÂM</strong></div>

<div style="clear:both;text-align:center">

<?php foreach($Pro_special as $product) { ?>   
    <div style="float:left;width:25%;height:325px;text-align:center" >
    <?php if($product["price_original"]>$product["price"]) { ?><div style="position:absolute;padding-left:23px"><img src="images/sale-label-product.png" width="55" height="55" alt="sale off" /></div><?php } ?>
    	<a href="san-pham/<?php echo strip_url($product["name"])."_".$product["cat_id"]."-".$product["id"]; ?>.html"><img src="<?php if($product["pic"]!="") echo "data/product/".$product["pic"]; else echo "images/shopbigsize-image-default.jpg"; ?>" width="180" height="180" style="padding:5px" /></a>
    	<br /><span style="font-size:12px;" class="text_black"><?php echo $product["name"]; ?></span>
        <div style="height:28px;padding-top:5px"><?php if($product["price_original"]>$product["price"]) { ?><span style="text-decoration:line-through;color:#390"><?php echo number_format($product["price_original"]); ?></span>&nbsp;&nbsp;&nbsp;<?php } ?><span class="text_red" style="font-size:16px"><?php echo number_format($product["price"]); ?> vnđ</span></div>
        
        <a class="popup_window" href="<?php if(isset($_SESSION['user'.SESSION_NAME])) echo "hien-thi/them-vao-danh-sach-yeu-thich.html?id=".$product["id"]; else echo "hien-thi/dang-nhap.html"; ?>"><img src="images/button-wishlist.gif" width="35" height="35" alt="wishlist" onmouseout="src='images/button-wishlist.gif'" onmouseover="src='images/button-wishlist-over.gif'" /></a>
        <a href="san-pham/<?php echo strip_url($product["name"])."_".$product["cat_id"]."-".$product["id"]; ?>.html"><img src="images/button-order.gif" width="77" height="35" alt="order"onmouseout="src='images/button-order.gif'" onmouseover="src='images/button-order-over.gif'" /></a>
    </div>
<?php } ?>    
    
</div>

<div style="clear:both;height:25px"></div>
