<?php
	if(isset($_SESSION['user'.SESSION_NAME]))header("Location: ".URL_PATH);

	if (isset($_POST['action']) and $_POST['action']=="register")
	{
		if ($_POST['security_code'] == $_SESSION['security_code'])
		{
			if($_POST['pass']!="")
			{	
				if ($_POST['pass']!=$_POST['pass_confirm'])
				{
					js_alert("Mật khẩu mới không giống mật khẩu xác nhận. Vui lòng nhập lại.");
					js_back(); exit;
				}		
				else
				{	
					$user = htmlspecialchars($_POST['user'], ENT_QUOTES|ENT_NOQUOTES);
					
					$message = check_email($user,"Tài khoản Email không hợp lệ, vui lòng nhập lại.");
					if($message!="")
					{
						js_alert($message);
					}
					else
					{
						$message = check_value_ready("user","`user` = '".$user."'","Tài khoản này đã có, vui lòng nhập lại.");
						if($message!="")
						{
							js_alert($message);
						}
						else
						{
							if($web_info["user_register"]=="A")
							{
								$email_active = "N";
								$email_code = rand(111111, 999999);
							}
							elseif ($web_info["user_register"]=="Y")
							{
								$email_active = "Y";
								$email_code = "";
							}
							else
							{
								exit;
							}
							
							$data = array(
								"user" 				=> $user,
								"pass" 				=> md5($_POST["pass"]),
								"register_date" 	=> date("Y-m-d H:i:s"),
								"register_ip" 		=> $_SERVER['REMOTE_ADDR'],
								"email_active" 		=> $email_active,
								"email_code" 		=> $email_code,								
								"name" 				=> $_POST["name"],
								"address" 			=> $_POST["address"],
								"city" 				=> $_POST["city"],
								"phone" 			=> $_POST["phone"],
								"author" 			=> $_POST["user"],
								"posted_date"		=> date("Y-m-d")		
							);
							insertData("user",$data);
	

							if($web_info["user_register"]=="A" and $email_active=="N")
							{
								$message = "
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
    <html xmlns='http://www.w3.org/1999/xhtml'>
    <head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
    <title>Thong Tin Dang Ky Thanh Vien</title>
    </head>
                
    <body>
              <div align='center'>
                <center>
                  <table border='0' cellpadding='5' cellspacing='0' style='border-collapse: collapse' bordercolor='#111111' width='100%'>
                    <tr>
                      <td width='100%' align='left' bgcolor='#91b7d4'>
                      <font face='Tahoma' style='font-size: 11pt'>Thông tin đăng ký tài khoản User trên Website ".URL_NAME."</font></td>
                    </tr>
                    <tr>
                      <td width='100%' bgcolor='#F2FEFF'>
                        <table border='0' cellpadding='5' cellspacing='0' style='border-collapse: collapse' bordercolor='#111111' width='100%'>
                                    
                            <tr>
                              <td colspan='3' align='left' bgcolor='#F2FEFF'><font style='font-size: 10pt; font-weight: 700' face='Tahoma'>Bạn đã đăng ký thành công với thông tin sau:</font></td>
                          </tr>
                              <tr bgcolor='#F2FEFF'>
                                <td width='8%'></td>
                                <td width='15%' align='left' vAlign='top'><span style='font-size: 10pt'><font face='Tahoma'>Tài khoản</font></span></td>
                                <td align='left' width='77%' valign='top'><font face='Tahoma' size='2' color='#ff8c00'><b>".$user."</b></font></td>
                          </tr>
                                <tr>
                                  <td bgcolor='#F2FEFF'></td>
                                  <td vAlign='top' align='left' bgcolor='#F2FEFF'><font face='Tahoma' style='font-size: 10pt; '>Mật khẩu</font></td>
                                  <td align='left' bgcolor='#F2FEFF' valign='top'><font face='Tahoma' size='2' color='#ff8c00'><b>".$_POST['pass']."</b></font></td>
                                </tr>
                                <tr bgcolor='#F2FEFF'>
                                  <td colspan='3' align='left'><font face='Tahoma' style='font-size: 10pt; font-weight:700'><a href='".URL_PATH."tai-khoan/kich-hoat-tai-khoan.html?user=".$_POST['user']."&email_code=".$email_code."'>Bấm vào đây</a> để kích hoạt tài khoản của bạn trước khi mua hàng trên Website ".URL_NAME."</font></td>
                          </tr>
                      </table>
					  </td>
                    </tr>
                  </table>
                </center>
        </div>
                    
                <p align='right'><font face='Tahoma' style='font-size: 8pt'>*This Email was sent automatically from ".URL_NAME." Website. Please don't reply.</font></p>
</body>
</html>				
								";
								
								$subject = "Thong tin kich hoat Tai Khoan User ".URL_NAME;
								$headers  = 'MIME-Version: 1.0' . "\r\n";
								$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
								$headers .= 'From: Ha Nam Anh <'.$web_info["contact_mail"].'>' . "\r\n";
								mail($_POST["user"], $subject, $message, $headers);									
								
								check_alert("Tài khoản đã được tạo thành công. Vui lòng check email của bạn để kích hoạt tài khoản.",URL_PATH);
				
							}
							else
							{
								$message = "
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<title>Thong Tin Dang Ky Thanh Vien</title>
</head>
			
<body>
		  <div align='center'>
			<center>
			  <table border='0' cellpadding='5' cellspacing='0' style='border-collapse: collapse' bordercolor='#111111' width='100%'>
				<tr>
				  <td width='100%' align='left' bgcolor='#91b7d4'><font face='Tahoma' style='font-size: 11pt'>Thông tin đăng ký tài khoản User trên Website ".URL_NAME."</font></td>
				</tr>
				<tr>
				  <td width='100%' bgcolor='#F2FEFF'>
					<table border='0' cellpadding='5' cellspacing='0' style='border-collapse: collapse' bordercolor='#111111' width='100%'>
								
						<tr>
						  <td colspan='3' align='left' bgcolor='#F2FEFF'><font style='font-size: 10pt; font-weight: 700' face='Tahoma'>Bạn đã đăng ký thành công  với thông tin sau:</font></td>
					  </tr>
						  <tr bgcolor='#F2FEFF'>
							<td width='8%'></td>
							<td width='17%' align='left' vAlign='top'><span class='style1' style='font-size: 10pt'><font face='Tahoma'>Tài khoản</font></span></td>
							<td align='left' width='75%' valign='top'><font face='Tahoma' size='2' color='#ff8c00'><b>".$user."</b></font></td>
					  </tr>
							<tr>
							  <td bgcolor='#F2FEFF'><span class='style1'></span></td>
							  <td vAlign='top' align='left' bgcolor='#F2FEFF'><font face='Tahoma' style='font-size: 10pt; '>Mật khẩu</font></td>
							  <td align='left' bgcolor='#F2FEFF' valign='top'><font face='Tahoma' size='2' color='#ff8c00'><b>".$_POST['pass']."</b></font></td>
							</tr>
							<tr bgcolor='#F2FEFF'>
							  <td colspan='3' align='left'><font face='Tahoma' style='font-size: 10pt; font-weight:700'><a href='".URL_PATH."'>Bấm vào đây</a> để bắt đầu niềm vui mua hàng tại ".URL_NAME." Website. Xin cảm ơn.</font></td>
					  </tr>
				  </table>				  </td>
				</tr>
			  </table>
			</center>
	</div>
				
			<p align='right'><font face='Tahoma' style='font-size: 8pt'>*This Email was sent automatically from ".URL_NAME." Website. Please don't reply.</font></p>
</body>
</html>				
								";
								
								$sql = "SELECT `id` FROM `user` WHERE `user`='".$_POST["user"]."' LIMIT 1";
								$user_id = getData($sql);
								$_SESSION['id'.SESSION_NAME] = $user_id["id"];
								$_SESSION['user'.SESSION_NAME] = $_POST["user"];
								
								$subject = "Thong tin dang ky tai khoan User ".URL_NAME;
								$headers  = 'MIME-Version: 1.0' . "\r\n";
								$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
								$headers .= 'From: Ha Nam Anh <'.$web_info["contact_mail"].'>' . "\r\n";
								mail($_POST["user"], $subject, $message, $headers);								
									
								if(@$_GET['status']=="order") $link = "dat-hang/thong-tin-mua-hang.html"; else $link = URL_PATH; 
								check_alert("Tài khoản đã được tạo thành công.",$link);
								exit;												
				
							}
						}
					}
				}
			}
		}
		else
		{
			$form_dangnhap_note = "Mã xác nhận không đúng";
			js_alert($form_dangnhap_note);
		}	
	}	

	// Lay info Tinh Thanh
	$sql = "SELECT `id`,`name` FROM `cat_thanhpho` ORDER BY `name` ASC,`id` DESC";
	$Tinhthanh = getDataInfo($sql);	
		
	require_once('module/user/view/register.tpl.php');
?>