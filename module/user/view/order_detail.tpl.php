<div style="clear:both;height:45px;line-height:45px;border-bottom:1px solid #dddddd;font-size:14px">
&nbsp;&nbsp;Trang chủ&nbsp;&nbsp;>&nbsp;&nbsp;Tài khoản&nbsp;&nbsp;>&nbsp;&nbsp;<span class="text_red">Lịch sử mua hàng</span>
</div>

<div style="clear:both;padding:30px">

<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="color:#1e557b;">
      <tr>
        <td height="30" colspan="6" align="center" style="border-bottom:1px solid #1e557b"><strong style="font-size:18px;color:#F00">Chi tiết Đơn Hàng số <?php echo $user["id"] ?></strong></td>
        </tr>
      <tr>
        <td colspan="6" align="center">&nbsp;</td>
      </tr>
      <tr>
        <td height="30" align="center">&nbsp;</td>
        <td height="30" align="left" valign="top"><strong>Người mua</strong></td>
        <td height="30" align="left"><strong><?php echo $user["name"]; ?></strong><br /><?php echo $user["address"]; ?><br />Tel: <?php echo $user["phone"]; ?></td>
        <td height="30" align="center">&nbsp;</td>
        <td height="30" align="center">&nbsp;</td>
        <td height="30" align="center">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="6" align="center">&nbsp;</td>
      </tr>
      <tr>
        <td height="30" align="center">&nbsp;</td>
        <td height="30" align="left" valign="top"><strong>Người nhận</strong></td>
        <td height="30" align="left"><strong>
          <?php if($user["name_to"]!="")echo $user["name_to"]; else echo $user["name"];?>
        </strong><br /><?php if($user["address_to"]!="")echo $user["address_to"]; else echo $user["address"] ?>
        <br />Tel: <?php if($user["phone_to"]!="")echo $user["phone_to"]; else echo $user["phone"]; ?></td>
        <td height="30" align="center">&nbsp;</td>
        <td height="30" align="center">&nbsp;</td>
        <td height="30" align="center">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="6" align="center">&nbsp;</td>
      </tr>
      <tr>
        <td height="40" align="center">&nbsp;</td>
        <td height="30" align="left" valign="top"><strong>Thanh toán</strong></td>
        <td height="30" align="left" valign="top"><strong><?php if($user["payment_type"]=="1") echo "bằng tiền mặt"; else echo "bằng chuyển khoản"; ?></strong></td>
        <td height="30" align="center">&nbsp;</td>
        <td height="30" align="center">&nbsp;</td>
        <td height="30" align="center">&nbsp;</td>
      </tr>
      <tr>
        <td height="30" align="center">&nbsp;</td>
        <td height="30" align="left" valign="top"><strong>Phí giao hàng</strong></td>
        <td height="30" align="left" valign="top"><strong><?php if($user["status"]=='0') echo "chờ cập nhật"; else echo number_format($user["shipping_fee"])." vnđ"; ?></strong></td>
        <td height="30" align="center">&nbsp;</td>
        <td height="30" align="center">&nbsp;</td>
        <td height="30" align="center">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="6" align="center">&nbsp;</td>
      </tr>
      <tr bgcolor="#b9d1e4">
        <td width="5%" height="30" align="center">&nbsp;</td>
        <td width="16%" align="center"><strong>Sản phẩm</strong></td>
        <td width="36%" align="center"><strong>Mô tả</strong></td>
        <td width="15%" align="center"><strong>Đơn giá</strong></td>
        <td width="12%" align="center"><strong>Số lượng</strong></td>
        <td width="16%" align="center"><strong>Thành tiền</strong></td>
      </tr>
<?php
foreach($Items as $product)
{
?>              
      <tr <?php if($stt%2==0) echo "style='background:#eff4f9;'";?>>
        <td height="110" align="right"><strong><?php echo $stt ?>.</strong></td>
        <td align="center"><div style="width:60px;height:72px;overflow:hidden;border:1px solid #91b7d4;"><img src="<?php echo URL_PATH ?>data/product/<?php echo $product["pic"]; ?>" width="60" height="72" style="padding:2px;"/></div></td>
        <td><span class="text_pink" style="font-size:12px;"><strong><?php echo $product["name"] ?></strong></span><br /><?php echo $product["detail"] ?></td>
        <td align="center"><?php echo number_format($product["price"]) ?></td>
        <td align="center"><input style="width:38px;height:22px;line-height:22px;text-align:center;color:#FF0000;vertical-align:middle;font-weight:bold;font-size:12px;" type="text"  value="<?php echo $product["quantity"] ?>" size="1" autocomplete="off" maxlength="3" readonly="readonly"/></td>
        <td align="right"><div style="padding-right:20px;"><?php echo number_format($total = $product["price"]*$product["quantity"])  ?></div></td>
      </tr>
      <tr>
        <td colspan="6" bgcolor="#b9d1e4" height="1"></td>
      </tr>
<?php $stt++; $total_cost = $total_cost + $total; } ?> 
     <tr>
        <td height="40" colspan="3">* Hỉnh ảnh có thể bị thay đổi hoặc đã xóa.</td>
        <td>&nbsp;</td>
        <td align="center"><strong>Tổng cộng</strong></td>
        <td align="right"><div style="padding-right:20px;"><strong><?php echo number_format($total_cost) ?></strong></div></td>
    </tr>
      <tr>
        <td colspan="6" bgcolor="#b9d1e4" height="2"></td>
      </tr>
      <tr>
        <td height="60" colspan="6"><div style="float:right"><a href="javascript:history.back();">Trở lại trang trước</a>&nbsp;&nbsp;<img src="images/icon_danhmuc.gif" /></div></td>
      </tr>
  </table>
      
</div>

<div style="clear:both;height:25px"></div>
