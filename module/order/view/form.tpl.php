<script language="javascript">
function chkForm(frm){
	if(frm.name.value=='')
	{alert("Vui lòng nhập Họ và Tên của bạn vào form.");frm.name.focus();return false;}
	if(frm.address.value=='')
	{alert("Vui lòng nhập Địa chỉ của bạn vào form.");frm.address.focus();return false;}
	if(frm.tel.value=='')
	{alert("Vui lòng nhập số Điện Thoại hoặc Di Động của bạn vào form.");frm.tel.focus();return false;}
	
	if(frm.copy.checked == false)
	{
		if(frm.name_to.value=='')
		{alert("Vui lòng nhập Họ và Tên người nhận vào form.");frm.name_to.focus();return false;}
		if(frm.address_to.value=='')
		{alert("Vui lòng nhập Địa chỉ người nhận vào form.");frm.address_to.focus();return false;}
		if(frm.tel_to.value=='')
		{alert("Vui lòng nhập số Điện Thoại hoặc Di Động của người nhận vào form.");frm.tel_to.focus();return false;}	
	}
	
	if(frm.shipping_info.selectedIndex=='0')
	{alert("Vui lòng chọn khu vực nhận hàng của bạn để chúng tôi phục vụ bạn tốt hơn.");return false;}	

	frm.submit();	
}
</script>

<div style="clear:both;height:45px;line-height:45px;border-bottom:1px solid #dddddd;font-size:14px">
&nbsp;&nbsp;Trang chủ&nbsp;&nbsp;>&nbsp;&nbsp;<span class="text_red">Thông tin đặt hàng</span>
</div>

<form name="frm" method="post" style="margin:0" enctype="multipart/form-data" onsubmit="return chkForm(this);">
<input type="hidden" name="action" value="true" />

<div style="padding:20px">

    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td></td>
      </tr>
      <tr>
        <td><table border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><a href="dat-hang/gio-hang.html"><img src="images/cart_1.gif" width="114" height="114" onmouseover="src='images/cart_1a.gif'" onmouseout="src='images/cart_1.gif'"/></a></td>
        <td><img src="images/cart_arrow.gif" width="40" height="46" /></td>
        <td><img src="images/cart_2a.gif" width="114" height="114"/></td>
        <td><img src="images/cart_arrow.gif" width="40" height="46" /></td>
        <td><img src="images/cart_3.gif" width="114" height="114"/></td>
        <td><img src="images/cart_arrow.gif" width="40" height="46" /></td>
        <td><img src="images/cart_4.gif" width="114" height="114"/></td>
      </tr>
      <tr>
        <td height="30" colspan="7">&nbsp;</td>
        </tr>
    </table></td>
      </tr>
      <tr>
        <td><table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td height="30" colspan="2" align="left"><strong class="text_blue" style="font-size:14px">Thông tin người mua hàng</strong></td>
        </tr>
      <tr>
        <td width="24%" height="30" align="left">Họ tên người mua *</td>
        <td width="76%" height="30" align="left"><input name="name" type="text" id="name" style="width:250px" value="<?php echo $user["name"]; ?>"/>          	</td>
      </tr>
      <tr>
        <td height="30" align="left">Địa chỉ người mua *</td>
        <td height="30" align="left">
          <input name="address" type="text" id="address" style="width:454px" value="<?php echo $user["address"].", ".$city["name"]; ?>"/>            </td>
      </tr>
      <tr>
        <td height="30" align="left">Điện thoại / Di động *</td>
        <td height="30" align="left"><input name="tel" id="tel" type="text" value="<?php echo $user["phone"]; ?>" style="width:200px"/></td>
      </tr>
      <tr>
        <td height="30" align="left">&nbsp;</td>
        <td height="30" align="left">&nbsp;</td>
      </tr>
      <tr align="left">
        <td height="30" colspan="2">
            <span><strong class="text_blue" style="font-size:14px">Thông tin người nhận hàng</strong></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <label><input name="copy" id="copy" type="checkbox" onclick="displayRow('to_1');displayRow('to_2');displayRow('to_3');"/>&nbsp;Nếu thông tin người nhận giống thông tin người mua</label></td>
        </tr>
      <tr id="to_1">
        <td width="24%" height="30" align="left">Họ tên người nhận</td>
        <td width="76%" align="left"><input name="name_to" id="name_to" type="text" style="width:250px"/>            </td></tr>
      <tr id="to_2">
        <td height="30" align="left">Địa chỉ người nhận</td>
        <td align="left"><input name="address_to" id="address_to" type="text" style="width:454px"/>            </td>
      </tr>
      <tr id="to_3">
        <td height="30" align="left">Điện thoại / Di động</td>
        <td align="left"><input name="tel_to" id="tel_to" type="text"  style="width:200px"/></td>
      </tr>
      <tr>
        <td height="30" align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr>
        <td height="30" align="left"><strong class="text_blue">Thông tin thanh toán</strong></td>
        <td align="left"><label><input name="payment" type="radio" id="payment" value="1" checked="checked" onclick="show_div('payment_1');hide_div('payment_2');"/> Giao hàng thu tiền tận nơi</label> &nbsp;&nbsp;&nbsp;&nbsp;<label><input type="radio" name="payment" id="payment" value="2" onclick="show_div('payment_2');hide_div('payment_1');"/> Chuyển khoản</label></td>
      </tr>
      <tr>
        <td height="30" align="left">&nbsp;</td>
        <td align="left" style="padding:10px;border:1px solid #CCCCCC;color:#333333"><span id="payment_1"><?php echo nl2br($web_info["thongtintaikhoan_1"]); ?></span>
          <span id="payment_2" style="display:none"><?php echo nl2br($web_info["thongtintaikhoan_2"]); ?></span></td>
      </tr>
      <tr>
        <td height="30" align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr>
        <td height="30" align="left"><strong class="text_blue">Khu vực giao hàng</strong></td>
        <td align="left">
          <select name="shipping_info" id="shipping_info" style="color:#000">
            <option value="0">Chọn khu vực giao hàng</option>
            <option value="Khu vực TP.HCM" onclick="show_div('shipping_1');hide_div('shipping_2');">Khu vực TP. HCM</option>
            <option value="Các Tỉnh Thành khác" onclick="show_div('shipping_2');hide_div('shipping_1');">Các tỉnh thành khác</option>
          </select>
        </td>
      </tr>
      <tr>
        <td height="30" align="left">&nbsp;</td>
        <td align="left" style="padding:10px;border:1px solid #CCCCCC;color:#333333">
<i>Cho4u sẽ báo phí giao hàng cụ thể khi confirm đơn hàng với quý khách</i>
<span id="shipping_1" style="display:none"><br /><strong style="color:#F00"><u>Cước phí tham khảo giao hàng tại TP. Hồ Chí Minh:</u></strong><br />
        <br />
            <strong style="color:#00F">Khu vực 1:</strong> gồm  quận 1, 2, 3, 4, 5, 6, 7, 8, 10, 11, Bình Thạnh, Tân Bình, Phú Nhuận, Tân Phú, Gò Vấp. Phí dự kiến là <strong>20.000vnđ</strong> / lần giao<br />
            <br />
            <strong style="color:#00F">Khu vực 2: </strong>gồm  quận 9, 12, Thủ Đức, Bình Tân. Phí dự kiến là <strong>30.000vnđ</strong> / lần giao<br />
            <br />
            <strong style="color:#00F">Khu vực 3: </strong>gồm các quận huyện vùng ven TPHCM như Nhà Bè, Hóc Môn, Bình Chánh, Củ Chi ... giao hàng bằng phương thức chuyển phát qua 1 ngày nhận hàng và tính phí của bưu điện VNPT. Phí dự kiến từ <strong>40.000vnđ</strong> trở xuống cho 01 lần giao.<br /><br />
</span>
<span id="shipping_2" style="display:none"><br /><strong style="color:#F00"><u>Cước phí tham khảo giao hàng các tỉnh thành ngoài TPHCM:</u></strong><br />
            <br />
            <strong style="color:#00F">SHIP nhanh: </strong>nhận hàng trong vòng 2 đến 3 ngày: giá từ <strong>40.000vnđ</strong> đến <strong>100.000vnđ</strong> tùy vào khối lượng hàng nhiều ít. Phí này  sẽ cộng vào tiền hàng thu hộ khi ra bưu điện cân hàng gửi. Vậy là khi nhận hàng, bạn vui lòng thanh toán cho nhân viên giao hàng sẽ gồm tiền hàng và tiền ship hàng về tận nơi.<br />
            <br />
            <strong style="color:#00F">SHIP thường:</strong> nhận hàng trong vòng 7 đến 10 ngày, phí ship thường rẻ hơn 1/4 đến 1/3 phí ship nhanh.
</span>
          </td>
      </tr>
      <tr>
        <td height="30" align="left">&nbsp;</td>
        <td align="left">&nbsp;</td>
      </tr>
      <tr>
        <td height="30" colspan="2" align="left"><input type="image" src="images/b_guidonhang.gif" width="145" height="35" align="right"/><a href="dat-hang/gio-hang.html"><img src="images/b_trolai.gif" width="193" height="35" align="left" /></a></td>
        </tr>
      <tr>
        <td height="30" colspan="2" align="left">&nbsp;</td>
      </tr>
    </table></td>
      </tr>
      <tr>
        <td height="50"></td>
      </tr>
    </table>

</div>
</form>