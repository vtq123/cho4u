<div style="clear:both;height:45px;line-height:45px;border-bottom:1px solid #dddddd;font-size:14px">
&nbsp;&nbsp;Trang chủ&nbsp;&nbsp;>&nbsp;&nbsp;Giỏ hàng&nbsp;&nbsp;>&nbsp;&nbsp;<span class="text_red">Các sản phẩm đã được chọn mua</span>
</div>


<div style="padding:20px">

    <form method="POST" id="cart" name="cart" action="dat-hang/gio-hang.html#area">
        <input name="action" type="hidden" value="update" />
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td></td>
          </tr>
          <tr>
            <td><table border="0" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td><img src="images/cart_1a.gif" width="114" height="114"/></td>
                <td><img src="images/cart_arrow.gif" width="40" height="46" /></td>
                <td><img src="images/cart_2.gif" width="114" height="114"/></td>
                <td><img src="images/cart_arrow.gif" width="40" height="46" /></td>
                <td><img src="images/cart_3.gif" width="114" height="114"/></td>
                <td><img src="images/cart_arrow.gif" width="40" height="46" /></td>
                <td><img src="images/cart_4.gif" width="114" height="114"/></td>
              </tr>
              <tr>
                <td height="30" colspan="7">&nbsp;</td>
                </tr>
            </table></td>
          </tr>
    <?php if(isset($_SESSION['cart']) and $total_cart>0){ ?>      
          <tr>
            <td><a name="area" id="area"></a>
                <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="color:#1e557b">
                  <tr bgcolor="#b9d1e4">
                    <td width="5%" height="30" align="center">&nbsp;</td>
                    <td width="15%" align="center"><strong>Sản phẩm</strong></td>
                    <td width="32%" align="center"><strong>Mô tả</strong></td>
                    <td width="13%" align="center"><strong>Đơn giá</strong></td>
                    <td width="13%" align="center"><strong>Số lượng</strong></td>
                    <td width="16%" align="center"><strong>Thành tiền</strong></td>
                    <td width="6%" align="center"><strong>Xóa</strong></td>
                  </tr>
    <?php
            foreach($_SESSION['cart'] as $key => $value)
            {
                $keys = explode("<<<>>>",$key);
                $id = $keys[0];
                $detail = $keys[1];
                $quantity = $value;
    
                $sql = "SELECT `id`,`name`,`price`,`price_original`,`pic` FROM `product` WHERE `id`='$id' LIMIT 1";	
                $product = getData($sql);	
    ?>              
                  <tr <?php if($stt%2==0) echo "style='background:#eff4f9;'";?>>
                    <td height="110" align="right"><strong><?php echo $stt ?>.</strong></td>
                    <td align="center"><img src="data/product/<?php echo $product["pic"] ?>" width="70" height="70" style="border:1px solid #91b7d4"/></td>
                    <td><strong style="font-size:12px;color:#06F"><?php echo $product["name"] ?></strong><?php if($detail!="") { ?><br />Ghi chú: <span style="color:#FF0000"><?php echo $detail ?></span><?php } ?>
					</td>
                    <td align="center">
						<?php if($product["price_original"] > $product["price"]) echo "<span style='text-decoration:line-through;color:#6C0'>".number_format($product["price_original"])."</span><br />"; ?><strong><?php echo number_format($product["price"]) ?></strong>
                    </td>
					<td align="center">
                    	<div class="pad_col_order"><input class="txt_quantity_order" type="text" name="num[<?php echo $key ?>]" onFocus="show_div('capnhat_<?php echo $key; ?>');" value="<?php echo $quantity ?>" size="1" autocomplete="off" maxlength="3" onkeypress="return checkIt(event);"/></div>
                        <div style="padding-top:5px;display:none;" id="capnhat_<?php echo $key ?>"><a href="javascript:updateCart();"><img src="images/icon_edit.gif" /></a></div></td>
                    <td align="right"><div style="padding-right:20px;"><?php echo number_format($total = $product["price"]*$quantity)  ?></div></td>
                    <td align="center"><a href="index.php?mod=order&amp;act=cart&action=delete&id=<?php echo $key ?>"><img src="images/icon_delete.gif" width="11" height="12" /></a></td>
                  </tr>
                  <tr>
                    <td colspan="7" bgcolor="#b9d1e4" height="1"></td>
                  </tr>
    <?php $stt++; $total_cost = $total_cost + $total; } ?> 
    
				  <tr>
                    <td height="40" colspan="5" align="right"><strong class="text_red">Mã giảm giá</strong>&nbsp;&nbsp;&nbsp;<input type="text" name="promotion_code" id="promotion_code" value="<?php echo @$_SESSION['promotion_code'] ?>" style="width:100px;padding:4px;text-align:center"/><input type="button" name="promotion_check" id="promotion_check" value="sử dụng" style="font-size:11px;" onclick="$check_promotion_code('index.php?mod=ajax&act=check_code','phieu_giam_gia')"/></td>
                    <td align="right"><div style="padding-right:20px;" id="phieu_giam_gia"><?php echo "- ".number_format($promotion_code_fee); ?></div></td>
                   <td align="center">&nbsp;</td>
                  </tr>    
    
                 
                  <tr>
                    <td height="30">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="center"><strong>Tổng cộng</strong></td>
                    <td align="right"><div style="padding-right:20px;font-weight:bold" id="total_cost"><?php echo number_format($total_cost - $promotion_code_fee) ?></div></td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="7" bgcolor="#b9d1e4" height="2"></td>
                  </tr>
                  <tr>
                    <td height="60" colspan="7" align="center" style="color:#000">Lưu ý: Đơn hàng trên chưa bao gồm <a href="thong-tin/chinh-sach-giao-nhan-hang_3.html" target="_blank" class="text_red">Phí Vận Chuyển</a>. Cho4u sẽ báo phí giao hàng cụ thể khi xác nhận đơn hàng với quý khách</td>
                  </tr>
                  <tr>
                    <td height="60" colspan="7"><a href="<?php if(isset($_SESSION['user'.SESSION_NAME])) echo "dat-hang/thong-tin-mua-hang.html"; else echo  'hien-thi/dang-nhap.html?link=order" class="popup_window"'; ?>"><img src="images/b_dathang.gif" width="116" height="35" align="right" /></a><a href="index.php"><img src="images/b_tieptucmuahang.gif" width="180" height="35" align="left" /></a></td>
                  </tr>
                </table>		</td>
          </tr>
    <?php } else { ?>      
          <tr>
            <td height="50" align="center"><p>&nbsp;</p>
              <p><strong style="font-size:14px;" class="text_red">Bạn chưa chọn sản phẩm nào.</strong></p>
              <p>&nbsp;</p>
              <p><a href="javascript:history.back();"><img src="images/b_trolai.gif" width="193" height="35" /></a></p></td>
          </tr>
    <?php } ?>      
          <tr>
            <td height="50"></td>
          </tr>
        </table>
        </form>

    </div>


<script type="text/javascript">
function updateCart(){
    document.cart.submit();
}
function checkIt(evt)//Khong cho nhap chu
{
	evt = (evt) ? evt : window.event
	var charCode = (evt.which) ? evt.which : evt.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46) {
		return false;
	}
	return true
}
function $check_promotion_code(url,id)
{
	if(document.cart.promotion_code.value=='')
		{alert("Vui lòng nhập Mã Giảm Giá vào form.");document.cart.promotion_code.focus();return false;}
		
	if(document.getElementById){var x=(window.ActiveXObject)?new ActiveXObject("Microsoft.XMLHTTP"):new XMLHttpRequest();}
	if(x){x.onreadystatechange=function() {
		el=document.getElementById(id);
		if(x.readyState==4&&x.status==200){
			el.innerHTML='';
			el=document.getElementById(id);
			el.innerHTML=x.responseText;
				var str = x.responseText.split("+++++");
				document.getElementById("phieu_giam_gia").innerHTML = "- " + str[0];
				document.getElementById("total_cost").innerHTML = str[1];
				if(str[0]==0) alert("Mã giảm giá không hợp lệ. Vui lòng nhập mã khác.");
			}
		}
		var code = document.getElementById('promotion_code').value;		
		x.open("GET",url+"&code="+code+"&total=<?php echo $total_cost ?>",true);
		x.send(null);
	}	
}
</script>

