<div style="clear:both;height:45px;line-height:45px;border-bottom:1px solid #dddddd;font-size:14px">
&nbsp;&nbsp;Trang chủ&nbsp;&nbsp;>&nbsp;&nbsp;<span class="text_red">Xác nhận lại đơn hàng lần cuối</span>
</div>


<div style="padding:20px">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td></td>
      </tr>
      <tr>
        <td><table border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td><a href="dat-hang/gio-hang.html"><img src="images/cart_1.gif" width="114" height="114" onmouseover="src='images/cart_1a.gif'" onmouseout="src='images/cart_1.gif'"/></a></td>
        <td><img src="images/cart_arrow.gif" width="40" height="46" /></td>
        <td><a href="dat-hang/thong-tin-mua-hang.html"><img src="images/cart_2.gif" width="114" height="114" onmouseover="src='images/cart_2a.gif'" onmouseout="src='images/cart_2.gif'"/></a></td>
        <td><img src="images/cart_arrow.gif" width="40" height="46" /></td>
        <td><img src="images/cart_3a.gif" width="114" height="114"/></td>
        <td><img src="images/cart_arrow.gif" width="40" height="46" /></td>
        <td><img src="images/cart_4.gif" width="114" height="114"/></td>
      </tr>
      <tr>
        <td height="30" colspan="7">&nbsp;</td>
        </tr>
    </table></td>
      </tr>
      <tr>
        <td><table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="color:#1e557b">
          <tr bgcolor="#b9d1e4">
            <td height="30" colspan="6" align="center" style="padding:5px;"><strong>Cảm ơn bạn đã mua hàng.<br />
              Xin vui lòng kiểm tra kỹ thông tin đặt hàng và hàng hóa truớc khi hoàn tất Đơn đặt hàng</strong></td>
          </tr>
          <tr>
            <td colspan="6" align="center">&nbsp;</td>
          </tr>
          <tr>
            <td height="30" align="center">&nbsp;</td>
            <td height="30" align="center" valign="top">Người mua</td>
            <td height="30" colspan="4" align="left"><strong><?php echo $user_info_0[0]; ?></strong><br /><?php echo $user_info_0[1]; ?><br /><?php echo $user_info_0[2]; ?></td>
          </tr>
          <tr>
            <td colspan="6" align="center">&nbsp;</td>
          </tr>
          <tr>
            <td height="30" align="center">&nbsp;</td>
            <td height="30" align="center" valign="top">Người nhận</td>
            <td height="30" colspan="4" align="left"><strong><?php echo $user_info_1[0] ?></strong><br /><?php echo $user_info_1[1] ?><br /><?php echo $user_info_1[2]; ?></td>
          </tr>
          <tr>
            <td colspan="6" align="center">&nbsp;</td>
          </tr>
          <tr>
            <td height="30" align="center">&nbsp;</td>
            <td height="30" align="center">Thanh toán</td>
            <td height="30" colspan="4" align="left"><strong><?php if($user_info_2[0]=="1") echo "bằng tiền mặt"; else echo "bằng chuyển khoản"; ?></strong></td>
          </tr>
          <tr>
            <td height="30" align="center">&nbsp;</td>
            <td height="30" align="center">Khu vực giao</td>
            <td height="30" colspan="4" align="left"><strong><?php echo $user_info_2[1]; ?></strong></td>
          </tr>              
          <tr>
            <td colspan="6" align="center">&nbsp;</td>
          </tr>
          <tr bgcolor="#b9d1e4">
            <td width="5%" height="30" align="center">&nbsp;</td>
            <td width="16%" align="center"><strong>Sản phẩm</strong></td>
            <td width="33%" align="center"><strong>Mô tả</strong></td>
            <td width="16%" align="center"><strong>Đơn giá</strong></td>
            <td width="12%" align="center"><strong>Số lượng</strong></td>
            <td width="18%" align="center"><strong>Thành tiền</strong></td>
          </tr>
<?php
    foreach($_SESSION['cart'] as $key => $value)
    {
        $keys = explode("<<<>>>",$key);
        $id = $keys[0];
        $detail = $keys[1];
        $quantity = $value;

        $sql = "SELECT `id`,`name`,`price`,`price_original`,`pic` FROM `product` WHERE `id`='$id' LIMIT 1";	
        $product = getData($sql);		
?>              
          <tr <?php if($stt%2==0) echo "style='background:#eff4f9;'";?>>
            <td height="110" align="right"><strong><?php echo $stt ?>.</strong></td>
            <td align="center"><img src="data/product/<?php echo $product["pic"] ?>" width="70" height="70" style="border:1px solid #91b7d4"/></td>
            <td><strong style="font-size:12px;color:#06F"><?php echo $product["name"] ?></strong><?php if($detail!="") { ?><br />Ghi chú: <span style="color:#FF0000"><?php echo $detail ?></span><?php } ?></td>
            <td align="center"><?php if($product["price_original"] > $product["price"]) echo "<span style='text-decoration:line-through;color:#6C0'>".number_format($product["price_original"])."</span><br />"; ?><strong><?php echo number_format($product["price"]) ?></strong></td>
            <td align="center"><input class="txt_quantity_order" type="text"  value="<?php echo $quantity ?>" size="1" autocomplete="off" maxlength="3" readonly="readonly"/></td>
            <td align="right"><div style="padding-right:20px;"><?php echo number_format($total = $product["price"]*$quantity)  ?></div></td>
          </tr>
          <tr>
            <td colspan="6" bgcolor="#b9d1e4" height="1"></td>
          </tr>
<?php $stt++; $total_cost = $total_cost + $total; } ?>  
          <tr>
            <td height="30">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="center"><strong>Mã giảm giá</strong></td>
            <td align="right"><div style="padding-right:20px;"><?php echo "- ".number_format($promotion_code_fee) ?></div></td>
          </tr>
          <tr>
            <td colspan="6" bgcolor="#b9d1e4" height="2"></td>
          </tr>            
          <tr>
            <td height="30">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="center"><strong>Tổng cộng</strong></td>
            <td align="right"><div style="padding-right:20px;"><strong><?php echo number_format($total_cost - $promotion_code_fee) ?></strong></div></td>
          </tr>
          <tr>
            <td colspan="6" bgcolor="#b9d1e4" height="2"></td>
          </tr>
          <tr>
            <td height="60" colspan="7" align="center" style="color:#000">Lưu ý: Đơn hàng trên chưa bao gồm <a href="thong-tin/chinh-sach-giao-nhan-hang_3.html" target="_blank" class="text_red">Phí Vận Chuyển</a>. Cho4u sẽ báo phí giao hàng cụ thể khi xác nhận đơn hàng với quý khách</td>
          </tr>
          <tr>
            <td height="60" colspan="6"><a href="dat-hang/hoan-tat.html"><img src="images/b_hoantatdathang.gif" align="right" /></a><a href="dat-hang/thong-tin-mua-hang.html"><img src="images/b_trolai.gif" align="left" /></a></td>
          </tr>
      </table></td>
      </tr>
      <tr>
        <td height="50"></td>
      </tr>
    </table>

</div>