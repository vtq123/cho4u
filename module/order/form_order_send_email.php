<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" style="color:#1e557b">
              <tr align="center" bgcolor="#b9d1e4">
                <td height="30" colspan="6" align="center"><strong>THÔNG TIN ĐƠN ĐẶT HÀNG</strong></td>
              </tr>
              <tr>
                <td colspan="6" align="center">&nbsp;</td>
              </tr>
              <tr>
                <td height="30" align="center">&nbsp;</td>
                <td height="30" align="center" valign="top">Người mua</td>
                <td height="30" align="left"><strong><?php echo $user_info_0[0]." - ".$user_info_0[1]; ?></strong><br />
                  <?php echo $user_info_0[2]." - ".$user_info_0[3]." - ".$user_info_0[4]; ?><br />
                  <?php echo $user_info_0[5]; ?></td>
                <td height="30" align="center">&nbsp;</td>
                <td height="30" align="center">&nbsp;</td>
                <td height="30" align="center">&nbsp;</td>
              </tr>
              <tr>
                <td colspan="6" align="center">&nbsp;</td>
              </tr>
              <tr>
                <td height="30" align="center">&nbsp;</td>
                <td height="30" align="center" valign="top">Người nhận</td>
                <td height="30" align="left"><strong><?php echo $user_info_1[0]." - ".$user_info_1[1]; ?></strong><br />
                  <?php echo $user_info_1[2] ?><br />
                  <?php echo $user_info_1[3]; ?></td>
                <td height="30" align="center">&nbsp;</td>
                <td height="30" align="center">&nbsp;</td>
                <td height="30" align="center">&nbsp;</td>
              </tr>
              <tr>
                <td colspan="6" align="center">&nbsp;</td>
              </tr>
              <tr>
                <td height="30" align="center">&nbsp;</td>
                <td height="30" align="center">Thanh toán</td>
                <td height="30" align="left"><strong><?php echo $user_info_2 ?></strong></td>
                <td height="30" align="center">&nbsp;</td>
                <td height="30" align="center">&nbsp;</td>
                <td height="30" align="center">&nbsp;</td>
              </tr>
              <tr>
                <td colspan="6" align="center">&nbsp;</td>
              </tr>
              <tr bgcolor="#b9d1e4">
                <td width="5%" height="30" align="center">&nbsp;</td>
                <td width="16%" align="center"><strong>Sản phẩm</strong></td>
                <td width="33%" align="center"><strong>Mô tả</strong></td>
                <td width="16%" align="center"><strong>Đơn giá</strong></td>
                <td width="12%" align="center"><strong>Số lượng</strong></td>
                <td width="18%" align="center"><strong>Thành tiền</strong></td>
              </tr>
<?php
		foreach($_SESSION['cart'] as $key => $value)
		{
			$keys = explode("_",$key);
			$id = $keys[0];
			$pic = "pic_".$keys[1];
			$type = "type_".$keys[1];
			$quantity = $value;

			$sql = "SELECT `id`,`name`,`code`,`price`,`$pic`,`$type` FROM `product` WHERE `id`='$id' LIMIT 1";	
			$product = getData($sql);	
?>              
              <tr <?php if($stt%2==0) echo "style='background:#eff4f9;'";?>>
                <td height="110" align="right"><strong><?php echo $stt ?>.</strong></td>
                <td align="center"><div style="width:76px;height:76px;overflow:hidden;border:1px solid #91b7d4;"><img src="<?php echo URL_PATH ?>/data/product/<?php echo $product["$pic"] ?>" width="72" height="72"/></div></td>
                <td align="left"><span class="text_red" style="font-size:12px;"><strong><?php echo $product["name"] ?></strong></span></td>
                <td align="center"><?php echo number_format($product["price"]) ?></td>
                <td align="center"><input class="txt_quantity_order" type="text"  value="<?php echo $quantity ?>" size="1" autocomplete="off" maxlength="3" readonly="readonly"/></td>
                <td align="right"><div style="padding-right:20px;"><?php echo number_format($total = $product["price"]*$quantity)  ?></div></td>
              </tr>
              <tr>
                <td colspan="6" bgcolor="#b9d1e4" height="1"></td>
              </tr>
<?php $stt++; $total_cost = $total_cost + $total; } ?>              
              <tr>
                <td height="30">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td align="center"><strong>Tổng cộng</strong></td>
                <td align="right"><div style="padding-right:20px;"><strong><?php echo number_format($total_cost) ?></strong></div></td>
              </tr>
              <tr>
                <td colspan="6" bgcolor="#b9d1e4" height="2"></td>
              </tr>
              <tr align="center">
                <td height="60" colspan="6"><strong>Cảm ơn bạn đã mua hàng. Chúng tôi sẽ liên lạc với bạn trong thời gian sớm nhất.<br>
                Nếu có gì thắc mắc, xin vui lòng liên hệ số điện thoại: 0916 589 772 / 0918 589 772</strong></td>
  </tr>
</table>
