<?php
	if(!isset($_SESSION['user'.SESSION_NAME]) or !isset($_SESSION['user']))header("Location: dat-hang/gio-hang.html");
	
	if(!isset($_SESSION['cart']) or !isset($_SESSION['user']))
	{
		header("Location: index.php");
		exit;
	}
	
	isset($_SESSION['promotion_code_fee'])	?	$promotion_code_fee = abs(intval($_SESSION['promotion_code_fee']))	:	$promotion_code_fee = 0;	

	// Info cua User
	$user_info_0 = explode("<<<>>>",$_SESSION['user'][0]);
	$user_info_1 = explode("<<<>>>",$_SESSION['user'][1]);
	$user_info_2 = explode("<<<>>>",$_SESSION['user'][2]);

	//Save Order
	$data = array(
		"user_id" 		=> $_SESSION['id'.SESSION_NAME],
		"status" 		=> "0", // 0-new,1-process,2-done,3-cancel
		"name" 			=> $user_info_0[0],
		"address" 		=> $user_info_0[1],
		"phone" 		=> $user_info_0[2],
		"email" 		=> $_SESSION['user'.SESSION_NAME],
		"name_to" 		=> $user_info_1[0],
		"address_to" 	=> $user_info_1[1],
		"phone_to" 		=> $user_info_1[2],
		"payment_type"	=> $user_info_2[0],
		"shipping_info"	=> $user_info_2[1],
		"promotion_code"		=> @$_SESSION['promotion_code'],
		"promotion_code_fee"	=> $promotion_code_fee,		
		"posted_date"	=> date("Y-m-d H:i:s"),
	);
	insertData("`order`",$data);
	
	// Lay id cua Order
	$sql = "SELECT `id` FROM `order` ORDER BY `id` DESC LIMIT 1";
	$order_id = getData($sql);

	
	if(@$_SESSION['promotion_code']!="")
	{
		$sql = "UPDATE `code` SET 
				`use_status` = 'Y',
				`use_email` = '".$_SESSION['user'.SESSION_NAME]."',
				`use_order` = '".$order_id["id"]."',
				`use_date`='".date("Y-m-d")."'
				WHERE `code`='".@$_SESSION['promotion_code']."'" ;
		myQuery($sql,$result);
	}

	// save order items
	foreach($_SESSION['cart'] as $key => $value)
	{
		$keys = explode("<<<>>>",$key);
		$id = $keys[0];
		$detail = $keys[1];
		$quantity = $value;

		$sql = "SELECT `name`,`code`,`pic`,`price`,`price_original` FROM `product` WHERE `id`='$id' LIMIT 1";	
		$product = getData($sql);
		
		$data_new = "";
		
		$data_new = array(
			"order_id" 		=> $order_id["id"],
			"product_id" 	=> $id,
			"name" 			=> $product["name"],
			"code" 			=> $product["code"],
			"pic" 			=> $product["pic"],
			"quantity" 		=> $quantity,
			"detail" 		=> $detail,
			"price" 		=> $product["price"],
			"price_original" 	=> $product["price_original"],
		);
		insertData("`order_items`",$data_new);	
	}
	
	//--- Send email
	$stt=1; $total_cost = 0;
	$admin_mail = $web_info["contact_mail"];
	
	if($user_info_2[0]=="1") $user_payment = "thanh toán bằng tiền mặt"; else $user_payment = "thanh toán bằng chuyển khoản";

	$message = "
			
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
<title>".URL_NAME."</title>
</head>
<body>
<table width='70%' border='0' align='center' cellpadding='0' cellspacing='0' style='color:#1e557b'>
              <tr align='center' bgcolor='#b9d1e4'>
                <td height='30' colspan='6' align='center'><strong>THÔNG TIN ĐƠN ĐẶT HÀNG TẠI ".URL_NAME."</strong></td>
              </tr>
              <tr>
                <td colspan='6' align='center'>&nbsp;</td>
              </tr>
              <tr>
                <td height='30' align='center'>&nbsp;</td>
                <td height='30' align='center' valign='top'>Người mua</td>
                <td height='30' align='left'><strong>".$user_info_0[0]."</strong><br />
                  ".$user_info_0[1]."<br />
                  ".$user_info_0[2]."</td>
                <td height='30' align='center'>&nbsp;</td>
                <td height='30' align='center'>&nbsp;</td>
                <td height='30' align='center'>&nbsp;</td>
              </tr>
              <tr>
                <td colspan='6' align='center'>&nbsp;</td>
              </tr>
              <tr>
                <td height='30' align='center'>&nbsp;</td>
                <td height='30' align='center' valign='top'>Người nhận</td>
                <td height='30' align='left'><strong>".$user_info_1[0]."</strong><br />
                  ".$user_info_1[1]."<br />
                  ".$user_info_1[2]."</td>
                <td height='30' align='center'>&nbsp;</td>
                <td height='30' align='center'>&nbsp;</td>
                <td height='30' align='center'>&nbsp;</td>
              </tr>
              <tr>
                <td colspan='6' align='center'>&nbsp;</td>
              </tr>
              <tr>
                <td height='30' align='center'>&nbsp;</td>
                <td height='30' align='center'>Thanh toán</td>
                <td height='30' align='left'><strong>".$user_payment."</strong></td>
                <td height='30' align='center'>&nbsp;</td>
                <td height='30' align='center'>&nbsp;</td>
                <td height='30' align='center'>&nbsp;</td>
              </tr>
              <tr>
                <td height='30' align='center'>&nbsp;</td>
                <td height='30' align='center'>Khu vực giao</td>
                <td height='30' align='left'><strong>".$user_info_2[1]."</strong></td>
                <td height='30' align='center'>&nbsp;</td>
                <td height='30' align='center'>&nbsp;</td>
                <td height='30' align='center'>&nbsp;</td>
              </tr>			  
              <tr>
                <td colspan='6' align='center'>&nbsp;</td>
              </tr>
              <tr bgcolor='#b9d1e4'>
                <td width='5%' height='30' align='center'>&nbsp;</td>
                <td width='16%' align='center'><strong>Sản phẩm</strong></td>
                <td width='33%' align='center'><strong>Mô tả</strong></td>
                <td width='16%' align='center'><strong>Đơn giá</strong></td>
                <td width='12%' align='center'><strong>Số lượng</strong></td>
                <td width='18%' align='right'><div style='padding-right:20px;'><strong>Thành tiền</strong></div></td>
              </tr>";

		foreach($_SESSION["cart"] as $key => $value)
		{
			$keys = explode("<<<>>>",$key);
			$id = $keys[0];
			$detail = $keys[1];
			$quantity = $value;
	
			$sql = "SELECT `id`,`name`,`price`,`price_original`,`pic` FROM `product` WHERE `id`='$id' LIMIT 1";	
			$product = getData($sql);
    
        $message .= "<tr ";
		if($stt%2==0) $message .= "style='background:#eff4f9;'";
		$message .= ">       
                <td height='110' align='right'><strong>".$stt.".</strong></td>
                <td align='center'><img src='".URL_PATH."data/product/".$product['pic']."' width='70' height='70' style='border:1px solid #91b7d4;'/></td>
                <td align='left'><span style='font-size:12px;color:#06F;'><strong>".$product['name']."</strong></span></td>
                <td align='center'>".number_format($product['price'])."</td>
                <td align='center'><strong>".$quantity."</strong></td>
                <td align='right'><div style='padding-right:20px;'>".number_format($total = $product['price']*$quantity) ."</div></td>
              </tr>
              <tr>
                <td colspan='6' bgcolor='#b9d1e4' height='1'></td>
              </tr>";
$stt++; $total_cost = $total_cost + $total; }
              
       $message .= "
	   		  <tr>
                <td height='30'>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td align='center'><strong>Giảm giá</strong></td>
                <td align='right'><div style='padding-right:20px;'><strong>- ".number_format($promotion_code_fee)."</strong></div></td>
              </tr>	   
	   		  <tr>
                <td height='30'>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td align='center'><strong>Tổng cộng</strong></td>
                <td align='right'><div style='padding-right:20px;'><strong>".number_format($total_cost - $promotion_code_fee)."</strong></div></td>
              </tr>
              <tr>
                <td colspan='6' bgcolor='#b9d1e4' height='2'></td>
              </tr>
              <tr align='center'>
                <td height='60' colspan='6'>Cảm ơn bạn đã mua hàng. Chúng tôi sẽ liên lạc với bạn trong thời gian sớm nhất.<br />Nếu có gì thắc mắc, xin vui lòng liên hệ Hotline: ".$web_info["phone_support"]."</td>
  </tr>
</table>
</body>
</html>
			";
	
	$subject = "Don Dat Hang ".URL_NAME;
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
	$headers .= 'To: <'.$_SESSION['user'.SESSION_NAME].'>' . "\r\n";
	$headers .= 'From: '.URL_NAME.' <'.$admin_mail.'>' . "\r\n";
	@mail($admin_mail, $subject, $message, $headers);
	
	//--- Xong roi, xoa session di
	unset($_SESSION['user']);
	unset($_SESSION['cart']);
	unset($_SESSION['promotion_code']);
	unset($_SESSION['promotion_code_fee']);
	
	require_once('module/order/view/complete.tpl.php');
?>